package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestCities
import com.company.sindirella.requestApi.RequestCitiesId
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement

class CityController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var citiesListener: CitiesListener? = null

    fun cities(orderSort: String?,orderName: String?,page: Int?){
        RequestCities(activity,fragment,orderSort,orderName,page,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                citiesListener?.getCities(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                citiesListener?.getCities(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                citiesListener?.getCities(false,null,failMessage,error)
            }

        })
    }

    fun cityId(id: String?){

        RequestCitiesId(activity,fragment,id,object: NetworkResponseListener<JsonElement> {
            override fun onResponseReceived(response: JsonElement) {
                citiesListener?.getCitiesId(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                citiesListener?.getCitiesId(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                citiesListener?.getCitiesId(false,null,-1,error)
            }
        })

    }


    interface CitiesListener{
        fun getCities(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
        fun getCitiesId(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }


}