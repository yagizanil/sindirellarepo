package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestGetComments
import com.company.sindirella.requestApi.RequestPostComments
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.util.ArrayList

class CommentController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var commentsListener: CommentsListener? = null
    var toCommentListener: ToCommentListener? = null

    fun comments(exist: Boolean?, hasTitle: Boolean?, hasTitleArray: ArrayList<Boolean>?,
                 isActive: Boolean?, isActiveArray: ArrayList<Boolean>?, product: String?, productArray: ArrayList<String>?, orderId: String?,
                 page: Int?){
        RequestGetComments(activity,fragment,exist,hasTitle,hasTitleArray,isActive,isActiveArray,product,productArray,orderId,page,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                commentsListener?.getComments(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                commentsListener?.getComments(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                commentsListener?.getComments(false,null,failMessage,error)
            }

        })
    }

    fun postComment(token: String?,body: JsonObject){
        RequestPostComments(activity,fragment,token,body,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                toCommentListener?.postToComment(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                toCommentListener?.postToComment(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                toCommentListener?.postToComment(false,null,failMessage,error)
            }

        })
    }



    interface CommentsListener{
        fun getComments(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }

    interface ToCommentListener{
        fun postToComment(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }


}