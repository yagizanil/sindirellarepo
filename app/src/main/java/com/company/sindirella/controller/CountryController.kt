package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestCountries
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement

class CountryController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var countriesListener: CountriesListener? = null

    fun countries(page: Int?){
        RequestCountries(activity,fragment,page,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                countriesListener?.getCountries(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                countriesListener?.getCountries(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                countriesListener?.getCountries(false,null,failMessage,error)
            }

        })
    }



    interface CountriesListener{
        fun getCountries(response: Boolean,jsonElement: JsonElement?,failMessage: Int?, error: ErrorResponse?)
    }



}