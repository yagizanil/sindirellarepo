package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestHelpPages
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import java.util.ArrayList

class HelpPageController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var helpPagesListener: HelpPagesListener? = null

    fun helpPages(code: String?, codeArray: ArrayList<String>?, page: Int?){

        RequestHelpPages(activity,fragment,code,codeArray,page,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                helpPagesListener?.getHelpPages(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                helpPagesListener?.getHelpPages(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                helpPagesListener?.getHelpPages(false,null,failMessage,error)
            }

        })

    }



    interface HelpPagesListener{
        fun getHelpPages(responseOk: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }


}