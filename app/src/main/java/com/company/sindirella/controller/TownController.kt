package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestTowns
import com.company.sindirella.requestApi.RequestTownsId
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement

class TownController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var townsListener: TownsListener? = null

    fun towns(page: Int?){
        RequestTowns(activity,fragment,page,object : NetworkResponseListener<JsonArray>{
            override fun onResponseReceived(response: JsonArray) {
                townsListener?.getTowns(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonArray?) {
                townsListener?.getTowns(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                townsListener?.getTowns(false,null,failMessage,error)
            }

        })
    }

    fun townsId(id: String?){

        RequestTownsId(activity,fragment,id,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                townsListener?.getTownsId(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                townsListener?.getTownsId(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                townsListener?.getTownsId(false,null,-1,error)
            }

        })

    }


    interface TownsListener{
        fun getTowns(response: Boolean?,jsonArray: JsonArray?,failMessage: Int?,error: ErrorResponse?)
        fun getTownsId(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }


}