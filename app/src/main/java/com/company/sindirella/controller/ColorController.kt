package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestColors
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement

class ColorController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var colorsListener: ColorsListener? = null

    fun colors(page: Int?){
        RequestColors(activity,fragment,page,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                colorsListener?.getColors(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                colorsListener?.getColors(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                colorsListener?.getColors(false,null,failMessage,error)
            }

        })
    }


    interface ColorsListener{
        fun getColors(responseOk: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }


}