package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponse
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.*
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.Order.*
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.util.ArrayList

class OrderController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {


    var activeOrdersListener: ActiveOrdersListener? = null

    fun getActiveOrders(token: String?,id: Int?, idArray: ArrayList<Int>?, status: Int?, statusArray: ArrayList<Int>?, orderId : String?, page : Int?){

        RequestActiveOrders(activity,fragment,token,id,idArray,status,statusArray,orderId,page,object : NetworkResponseListener<JsonElement> {
            override fun onResponseReceived(response: JsonElement) {
                activeOrdersListener?.getActiveOrders(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                activeOrdersListener?.getActiveOrders(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                activeOrdersListener?.getActiveOrders(false,null,failMessage,error)
            }

        })
    }


    fun getOrderHistory(id: Int?, idArray: ArrayList<Int>?, status: Int?, statusArray: ArrayList<Int>?, orderId : String?, page : Int?){

        RequestOrderHistory(activity,fragment,id,idArray,status,statusArray,orderId,page,object : NetworkResponseListener<JsonArray> {
            override fun onResponseReceived(response: JsonArray) {
                activeOrdersListener?.getOrderHistory(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonArray?) {
                activeOrdersListener?.getOrderHistory(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                activeOrdersListener?.getOrderHistory(false,null,failMessage,error)
            }

        })
    }

    fun postOrders(body: JsonObject?){
        RequestOrders(activity,fragment,body,object : NetworkResponseListener<OrdersResponse> {
            override fun onResponseReceived(response: OrdersResponse) {

            }

            override fun onEmptyResponse(response: OrdersResponse?) {

            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {

            }

        })
    }

    fun GetOrdersId(id: String?){
        RequestGetOrdersId(activity,fragment,id,object : NetworkResponseListener<GetOrdersIdResponse> {
            override fun onResponseReceived(response: GetOrdersIdResponse) {

            }

            override fun onEmptyResponse(response: GetOrdersIdResponse?) {

            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {

            }

        })
    }

    fun PutOrdersId(id: String?,body: JsonObject?){
        RequestPutOrdersId(activity,fragment,id,body,object : NetworkResponseListener<PutOrdersIdResponse> {
            override fun onResponseReceived(response: PutOrdersIdResponse) {

            }

            override fun onEmptyResponse(response: PutOrdersIdResponse?) {

            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {

            }

        })
    }

    fun DeleteOrdersId(id: String?){
        RequestDeleteOrdersId(activity,fragment,id,object : NetworkResponseListener<PutOrdersIdResponse> {
            override fun onResponseReceived(response: PutOrdersIdResponse) {

            }

            override fun onEmptyResponse(response: PutOrdersIdResponse?) {

            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {

            }

        })
    }



    interface ActiveOrdersListener{
        fun getActiveOrders(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
        fun getOrderHistory(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }

}