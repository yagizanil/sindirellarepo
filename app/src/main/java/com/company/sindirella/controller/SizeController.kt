package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestSizes
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import java.util.ArrayList

class SizeController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var sizesListener: SizesListener? = null

    fun sizes(country: String?, countryArray: ArrayList<String>?, page: Int?){
        RequestSizes(activity,fragment,country,countryArray,page,object: NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                sizesListener?.getSizes(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                sizesListener?.getSizes(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                sizesListener?.getSizes(false,null,failMessage,error)
            }

        })
    }


    interface SizesListener{
        fun getSizes(responseOk: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }

}