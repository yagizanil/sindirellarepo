package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.activitys.MainActivity
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestBrands
import com.company.sindirella.requestApi.RequestDynamicApi
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement

class DynamicApiController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var dynamicApiListener: DynamicApiListener? = null

    fun dynamicApi(url: String,orderPopulartiy: String?,orderPriceRental: String?,orderId: String?,activities: String?){

        RequestDynamicApi(activity,fragment,url,orderPopulartiy,orderPriceRental,orderId,activities,object : NetworkResponseListener<JsonElement> {
            override fun onResponseReceived(response: JsonElement) {
                dynamicApiListener?.getDynamicApi(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                dynamicApiListener?.getDynamicApi(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                dynamicApiListener?.getDynamicApi(false,null,failMessage,error)
            }


        })
    }


    interface DynamicApiListener{
        fun getDynamicApi(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?)
    }


}