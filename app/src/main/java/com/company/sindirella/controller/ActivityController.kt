package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestActivities
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement

class ActivityController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var activitiesListener: ActivitiesListener? = null

    fun activities(page: Int?){

        RequestActivities(activity,fragment,page,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                activitiesListener?.getActivities(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                activitiesListener?.getActivities(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                activitiesListener?.getActivities(false,null,failMessage,error)
            }

        })

    }

    interface ActivitiesListener {
        fun getActivities(response: Boolean?,jsonElement: JsonElement?,failMessage: Int?, error: ErrorResponse?)
    }



}