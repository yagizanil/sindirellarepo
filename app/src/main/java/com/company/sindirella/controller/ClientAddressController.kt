package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestDeleteAddressesId
import com.company.sindirella.requestApi.RequestGetClientAddresses
import com.company.sindirella.requestApi.RequestPostClientAddresses
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class ClientAddressController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var clientAddressesListener: ClientAddressesListener? = null

    fun postClientAddresses(token: String?,body: JsonObject?){
        RequestPostClientAddresses(activity,fragment,token,body,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                clientAddressesListener?.postClientAddresses(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                clientAddressesListener?.postClientAddresses(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                clientAddressesListener?.postClientAddresses(false,null,failMessage,error)
            }

        })
    }



    fun getClientAddresses(token: String?,page: Int?){
        RequestGetClientAddresses(activity,fragment,token,page,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                clientAddressesListener?.getClientAddresses(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                clientAddressesListener?.getClientAddresses(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                clientAddressesListener?.getClientAddresses(false,null,failMessage,error)
            }

        })
    }

    fun deleteClientAddreses(token: String?,id: String?){
        RequestDeleteAddressesId(activity,fragment,token,id,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                clientAddressesListener?.deleteClientAddresses(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                clientAddressesListener?.deleteClientAddresses(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                clientAddressesListener?.deleteClientAddresses(false,null,-1,error)
            }

        })
    }


    interface ClientAddressesListener{
        fun postClientAddresses(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
        fun getClientAddresses(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
        fun deleteClientAddresses(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }


}