package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestSizeGroup
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement

class SizeGroupController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var sizeGroupListener: SizeGroupListener? = null

    fun sizeGroup(page: Int?){
        RequestSizeGroup(activity,fragment,page,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                sizeGroupListener?.getSizeGroup(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                sizeGroupListener?.getSizeGroup(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                sizeGroupListener?.getSizeGroup(false,null,failMessage,error)
            }

        })
    }


    interface SizeGroupListener{
        fun getSizeGroup(responseOk: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }


}