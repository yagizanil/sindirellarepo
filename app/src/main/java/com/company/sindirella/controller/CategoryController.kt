package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestCategories
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement

class CategoryController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var categoriesListener: CategoriesListener? = null

    fun categories(exist: Boolean?,page: Int?){
        RequestCategories(activity,fragment,exist,page,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                categoriesListener?.getCategories(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                categoriesListener?.getCategories(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                categoriesListener?.getCategories(false,null,failMessage,error)
            }

        })
    }


    interface CategoriesListener{
        fun getCategories(responseOk: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }

}