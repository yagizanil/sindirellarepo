package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Comment.GetCommentsResponse
import com.company.sindirella.response.Comment.PostCommentsResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import retrofit2.http.Query
import java.util.ArrayList

class RequestGetComments(activity: AppCompatActivity?, fragment: Fragment?, exist: Boolean?, hasTitle: Boolean?, hasTitleArray: ArrayList<Boolean>?,
                         isActive: Boolean?, isActiveArray: ArrayList<Boolean>?, product: String?, productArray: ArrayList<String>?, orderId: String?,
                         page: Int?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.GetComments>(Service.GetComments::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getComments(exist,hasTitle,hasTitleArray,isActive,isActiveArray,product,productArray,orderId,page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}