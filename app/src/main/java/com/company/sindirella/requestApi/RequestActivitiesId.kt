package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Activity.ActivitysIdResponse
import com.company.sindirella.response.Activity.ActivitysResponse
import java.util.ArrayList

class RequestActivitiesId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<ActivitysIdResponse>) {

    init {
        val request = RequestCreator.create<Service.ActivitiesId>(Service.ActivitiesId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getActivitiesId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}