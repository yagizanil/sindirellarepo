package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Payment.GetPaymentIdResponse
import com.company.sindirella.response.Payment.PostPaymentResponse
import com.google.gson.JsonObject

class RequestGetPaymentId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<GetPaymentIdResponse>) {

    init {
        val request = RequestCreator.create<Service.GetPaymentId>(Service.GetPaymentId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getPaymentId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}