package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Brand.BrandsIdResponse
import com.company.sindirella.response.Brand.BrandsResponse
import java.util.ArrayList

class RequestBrandsId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<BrandsIdResponse>) {

    init {
        val request = RequestCreator.create<Service.BrandsId>(Service.BrandsId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getBrandsId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}