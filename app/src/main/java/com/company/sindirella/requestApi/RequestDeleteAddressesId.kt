package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.google.gson.JsonElement

class RequestDeleteAddressesId(activity: AppCompatActivity?, fragment: Fragment?, token: String?, id: String?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.DeleteClientAddressesId>(Service.DeleteClientAddressesId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.deleteClientAddressesId(token,id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}