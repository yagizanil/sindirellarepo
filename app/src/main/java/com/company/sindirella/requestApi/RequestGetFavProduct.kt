package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.District.DistrictIdResponse
import com.company.sindirella.response.FavProduct.GetFavProductResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import java.util.ArrayList

class RequestGetFavProduct(activity: AppCompatActivity?, fragment: Fragment?, token: String?, page: Int?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.GetFavProduct>(Service.GetFavProduct::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getFavProduct(token,page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}