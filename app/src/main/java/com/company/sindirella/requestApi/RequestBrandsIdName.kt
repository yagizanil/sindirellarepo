package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Brand.BrandsIdNameResponse
import com.company.sindirella.response.Brand.BrandsIdResponse

class RequestBrandsIdName(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<BrandsIdNameResponse>) {

    init {
        val request = RequestCreator.create<Service.BrandsIdName>(Service.BrandsIdName::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getBrandsIdName(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}