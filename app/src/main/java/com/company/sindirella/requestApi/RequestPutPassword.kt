package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Client.PutMeResponse
import com.company.sindirella.response.Client.PutPasswordResponse
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class RequestPutPassword(activity: AppCompatActivity?, fragment: Fragment?,token : String?, id: String?, body: JsonObject?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.PutPassword>(Service.PutPassword::class.java, NetworkSupport.NetworkAdress.base_url)
        request.putPassword(token,id,body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}