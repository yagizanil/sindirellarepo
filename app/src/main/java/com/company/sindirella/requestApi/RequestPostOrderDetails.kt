package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.OrderDetail.PostOrderDetailResponse
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.util.ArrayList

class RequestPostOrderDetails(activity: AppCompatActivity?, fragment: Fragment?, body: JsonObject?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.PostOrderDetails>(Service.PostOrderDetails::class.java, NetworkSupport.NetworkAdress.base_url)
        request.postOrderDetails(body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}