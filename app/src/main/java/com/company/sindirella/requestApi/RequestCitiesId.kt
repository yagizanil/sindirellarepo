package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.City.CitiesIdResponse
import com.company.sindirella.response.City.CitiesResponse
import com.google.gson.JsonElement
import java.util.ArrayList

class RequestCitiesId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.CitiesId>(Service.CitiesId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getCitiesId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}