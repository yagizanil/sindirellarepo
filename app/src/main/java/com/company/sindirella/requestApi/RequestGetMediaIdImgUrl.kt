package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Media.GetMediaIdImgUrlResponse
import com.company.sindirella.response.Media.GetMediaIdResponse

class RequestGetMediaIdImgUrl(activity: AppCompatActivity?, fragment: Fragment?, id : String?, listener: NetworkResponseListener<GetMediaIdImgUrlResponse>) {

    init {
        val request = RequestCreator.create<Service.GetMediaIdImgUrl>(Service.GetMediaIdImgUrl::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getMediaIdImgUrl(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}