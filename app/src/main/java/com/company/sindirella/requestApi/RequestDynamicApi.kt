package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.helpers.GlobalData
import com.company.sindirella.network.*
import com.company.sindirella.response.District.DistrictResponse
import com.google.gson.JsonElement
import java.util.ArrayList

class RequestDynamicApi(activity: AppCompatActivity?, fragment: Fragment?, url: String,orderPopulartiy: String?,orderPriceRental: String?,orderId: String?,activities: String?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.DynamicApi>(Service.DynamicApi::class.java, GlobalData.base_url)
        request.getDynamicApi(url,orderPopulartiy,orderPriceRental,orderId,activities).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}