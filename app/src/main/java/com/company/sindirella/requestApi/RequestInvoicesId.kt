package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Invoice.InvoiceIdResponse
import com.company.sindirella.response.Invoice.InvoiceResponse
import java.util.ArrayList

class RequestInvoicesId(activity: AppCompatActivity?, fragment: Fragment?, id : String?, listener: NetworkResponseListener<InvoiceIdResponse>) {

    init {
        val request = RequestCreator.create<Service.InvoicesId>(Service.InvoicesId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getInvoicesId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}