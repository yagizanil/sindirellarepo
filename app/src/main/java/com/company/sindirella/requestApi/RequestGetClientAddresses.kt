package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.ClientAddress.GetClientAddressResponse
import com.company.sindirella.response.ClientAddress.PostClientAddressResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.util.ArrayList

class RequestGetClientAddresses(activity: AppCompatActivity?, fragment: Fragment?,token: String?, page: Int?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.GetClientAddresses>(Service.GetClientAddresses::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getClientAddresses(token,page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}