package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Client.PostRegisterResponse
import com.company.sindirella.response.Order.ActiveOrdersResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import retrofit2.http.Query
import java.util.ArrayList

class RequestActiveOrders(activity: AppCompatActivity?, fragment: Fragment?,token: String?, id: Int?, idArray: ArrayList<Int>?, status: Int?, statusArray: ArrayList<Int>?, orderId : String?
                          , page : Int?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.ActiveOrders>(Service.ActiveOrders::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getActiveOrders(token,id,idArray,status,statusArray,orderId,page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}