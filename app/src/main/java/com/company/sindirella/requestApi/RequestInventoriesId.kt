package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Invertory.InventoriesIdResponse
import com.company.sindirella.response.Invertory.InventoriesResponse
import java.util.ArrayList

class RequestInventoriesId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<InventoriesIdResponse>) {

    init {
        val request = RequestCreator.create<Service.InventoriesId>(Service.InventoriesId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getInventoriesId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}