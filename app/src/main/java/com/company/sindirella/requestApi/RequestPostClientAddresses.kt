package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.City.CitiesIdResponse
import com.company.sindirella.response.ClientAddress.PostClientAddressResponse
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class RequestPostClientAddresses(activity: AppCompatActivity?, fragment: Fragment?,token: String?, body: JsonObject?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.PostClientAddresses>(Service.PostClientAddresses::class.java, NetworkSupport.NetworkAdress.base_url)
        request.postClientAddresses(token,body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}