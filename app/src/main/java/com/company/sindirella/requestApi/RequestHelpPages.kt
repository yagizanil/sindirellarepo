package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.FavProduct.FavProductIdResponse
import com.company.sindirella.response.HelpPage.HelpPageResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import retrofit2.http.Query
import java.util.ArrayList

class RequestHelpPages(activity: AppCompatActivity?, fragment: Fragment?, code: String?, codeArray: ArrayList<String>?, page: Int?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.HelpPages>(Service.HelpPages::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getHelpPages(code,codeArray,page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}