package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Client.GetMeResponse
import com.company.sindirella.response.Client.PutMeResponse
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class RequestPutMe(activity: AppCompatActivity?, fragment: Fragment?, token: String?,body: JsonObject?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.PutMe>(Service.PutMe::class.java, NetworkSupport.NetworkAdress.base_url)
        request.putMe(token,body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}