package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.District.DistrictIdResponse
import com.company.sindirella.response.District.DistrictResponse
import java.util.ArrayList

class RequestDistrictsId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<DistrictIdResponse>) {

    init {
        val request = RequestCreator.create<Service.DistrictsId>(Service.DistrictsId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getDistrictsId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}