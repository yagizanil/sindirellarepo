package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Size.SizeIdResponse
import com.company.sindirella.response.Tag.TagResponse
import java.util.ArrayList

class RequestTags(activity: AppCompatActivity?, fragment: Fragment?, page: Int?, listener: NetworkResponseListener<ArrayList<TagResponse>>) {

    init {
        val request = RequestCreator.create<Service.Tags>(Service.Tags::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getTags(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}