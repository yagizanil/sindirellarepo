package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Product.ProductIdResponse
import com.company.sindirella.response.PromotionArea.PromotionAreaResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import java.util.ArrayList

class RequestPromotionAreas(activity: AppCompatActivity?, fragment: Fragment?, page: Int?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.PromotionAreas>(Service.PromotionAreas::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getPromotionAreas(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}