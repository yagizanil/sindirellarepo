package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.BasketItem.BasketItemsIdResponse
import com.company.sindirella.response.Brand.BrandsResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import java.util.ArrayList

class RequestBrands(activity: AppCompatActivity?, fragment: Fragment?, page: Int?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.Brands>(Service.Brands::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getBrands(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}