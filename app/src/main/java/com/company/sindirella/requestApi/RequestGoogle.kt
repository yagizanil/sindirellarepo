package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Client.ClientsIdResponse
import com.company.sindirella.response.Client.PostGoogleResponse
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class RequestGoogle(activity: AppCompatActivity?, fragment: Fragment?, body: JsonObject?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.Google>(Service.Google::class.java, NetworkSupport.NetworkAdress.base_url)
        request.postGoogle(body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}