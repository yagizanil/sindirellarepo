package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Color.ColorsIdResponse
import com.company.sindirella.response.Color.ColorsResponse
import java.util.ArrayList

class RequestColorsId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<ColorsIdResponse>) {

    init {
        val request = RequestCreator.create<Service.ColorsId>(Service.ColorsId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getColorsId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}