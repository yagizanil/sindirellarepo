package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Client.PutPasswordResponse
import com.company.sindirella.response.Color.ColorsResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.util.ArrayList

class RequestColors(activity: AppCompatActivity?, fragment: Fragment?, page: Int?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.Colors>(Service.Colors::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getColors(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}