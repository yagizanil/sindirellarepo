package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.FavProduct.FavProductIdResponse
import com.company.sindirella.response.FavProduct.PostFavProductResponse
import com.google.gson.JsonObject

class RequestGetFavProductId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<FavProductIdResponse>) {

    init {
        val request = RequestCreator.create<Service.GetFavProductId>(Service.GetFavProductId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getFavProductId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}