package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.OrderDetail.GetOrderDetailIdResponse
import com.company.sindirella.response.OrderDetail.PostOrderDetailResponse
import com.google.gson.JsonObject

class RequestGetOrderDetailsId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<GetOrderDetailIdResponse>) {

    init {
        val request = RequestCreator.create<Service.GetOrderDetailsId>(Service.GetOrderDetailsId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getOrderDetailsId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}