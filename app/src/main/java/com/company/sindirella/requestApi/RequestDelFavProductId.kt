package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.FavProduct.FavProductIdResponse
import com.google.gson.JsonElement

class RequestDelFavProductId(activity: AppCompatActivity?, fragment: Fragment?,token: String?, id: String?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.DeleteFavProductId>(Service.DeleteFavProductId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.deleteFavProductId(token,id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}