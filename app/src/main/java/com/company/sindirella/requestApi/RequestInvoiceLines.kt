package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.InvoiceLine.InvoiceLineResponse
import com.company.sindirella.response.Order.ActiveOrdersResponse
import java.util.ArrayList

class RequestInvoiceLines(activity: AppCompatActivity?, fragment: Fragment?,page : Int?, listener: NetworkResponseListener<ArrayList<InvoiceLineResponse>>) {

    init {
        val request = RequestCreator.create<Service.InvoiceLines>(Service.InvoiceLines::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getInvoiceLines(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}