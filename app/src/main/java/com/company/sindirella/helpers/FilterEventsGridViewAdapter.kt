package com.company.sindirella.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.TextView
import com.company.sindirella.R
import com.company.sindirella.response.Activity.ActivitysResponseHydraMember

class FilterEventsGridViewAdapter(var context: Context,var eventsData: ArrayList<ActivitysResponseHydraMember>) : BaseAdapter() {

    private var checkBoxClick: CheckBox? = null
    private var eventName: TextView? = null

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view = LayoutInflater.from(context).inflate(R.layout.filter_events_grid_view,parent,false)

        checkBoxClick = view.findViewById(R.id.checkBoxClick)
        eventName = view.findViewById(R.id.eventName)

        eventName?.text = eventsData.get(position).name?.toUpperCase()




        return view

    }

    override fun getItem(position: Int): Any {
        return eventsData.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return eventsData.size
    }


}