package com.company.sindirella.helpers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.company.sindirella.R
import java.util.ArrayList
import top.defaults.drawabletoolbox.DrawableBuilder

class FilterActivityGridViewAdapter(private val context: Context,private val data : ArrayList<String>) : BaseAdapter() {

    private var textViewSize : TextView? = null
    private var mainLayout : LinearLayout? = null

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(context).inflate(R.layout.filter_gridview_item_view,parent,false)

        textViewSize = view.findViewById(R.id.textViewSize)
        mainLayout = view.findViewById(R.id.mainLayout)


        textViewSize?.text = data.get(position)



        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).cornerRadius(20).build()

        return view
    }

    override fun getItem(position: Int): Any {
        return data.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return data.size
    }


}