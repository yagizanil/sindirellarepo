package com.company.sindirella.helpers

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.company.sindirella.R
import com.company.sindirella.activitys.LoginActivity
import com.company.sindirella.appPreferences
import com.company.sindirella.fragments.BrowseFragment
import com.company.sindirella.response.Product.ProductResponse
import java.util.ArrayList
import top.defaults.drawabletoolbox.DrawableBuilder

class BrowseFragmentRecycleAdapter(private val context: Context?,private var productResponse: ProductResponse?) : RecyclerView.Adapter<BrowseFragmentRecycleAdapter.ViewHolder>() {


    private var items: ArrayList<String> = ArrayList()

    private var mainLayout: RelativeLayout? = null
    private var clothesView: RelativeLayout? = null
    private var imageViewClothes: ImageView? = null
    private var imageLike: ImageView? = null
    private var brandName: TextView? = null
    private var clothesName: TextView? = null
    private var priceLayout: LinearLayout? = null
    private var pricesNew: TextView? = null
    private var viewLinePrices: View? = null
    private var pricesOld: TextView? = null

    var clickRcycleItemListener : ClickRcycleItemListener? = null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.browse_viewpager_item_view, parent, false)


        clothesView = v.findViewById(R.id.clothesView)
        imageViewClothes = v.findViewById(R.id.imageViewClothes)
        imageLike = v.findViewById(R.id.imageLike)
        brandName = v.findViewById(R.id.brandName)
        clothesName = v.findViewById(R.id.clothesName)
        priceLayout = v.findViewById(R.id.priceLayout)
        pricesNew = v.findViewById(R.id.pricesNew)
        viewLinePrices = v.findViewById(R.id.viewLinePrices)
        pricesOld = v.findViewById(R.id.pricesOld)
        mainLayout = v.findViewById(R.id.mainLayout)




        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.white)).cornerRadius(50).build()

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var requestOptions = RequestOptions()
        requestOptions.transform(CenterCrop(), RoundedCorners(50))

        Glide.with(context!!).load(productResponse?.hydraMember?.get(position)?.pictures?.get(0)?.imageUrl).apply(requestOptions).into(imageViewClothes!!)
        //imageViewClothes?.background = DrawableBuilder().cornerRadii(50,50,0,0).build()
        brandName?.text = productResponse?.hydraMember?.get(position)?.brand?.name

        var lengthName = productResponse?.hydraMember?.get(position)?.name?.length

        /*if (lengthName!! > 20){

            clothesName?.textSize = 13f

        }else if (lengthName > 30){

            clothesName?.textSize = 12f

        }

         */

        clothesName?.text = productResponse?.hydraMember?.get(position)?.name
        pricesNew?.text = productResponse?.hydraMember?.get(position)?.priceRental.toString() + " ₺"
        pricesOld?.text = productResponse?.hydraMember?.get(position)?.priceSale.toString() + " ₺"
        pricesOld?.paintFlags = (Paint.STRIKE_THRU_TEXT_FLAG)

        mainLayout?.setOnClickListener {

            clickRcycleItemListener?.clickRcycleItemListener(productResponse?.hydraMember?.get(position)?.BigId.toString())
            //popularClick?.popularClick(productResponse.hydraMember?.get(position)?.BigId)

        }

        imageLike?.setOnClickListener {

            if (appPreferences.token.equals("")){
                val intent = Intent(context,LoginActivity::class.java)
                context.startActivity(intent)
            }else{

            }

        }

        holder.setIsRecyclable(false)
    }

    override fun getItemCount(): Int {

        return productResponse?.hydraMember?.size!!

    }

    fun add(stringVal: String) {

        /*if (preferencesQuery.id == -1){

        }else{
            items.add(preferencesQuery)
            filteredList.add(preferencesQuery)
            notifyDataSetChanged()
        }*/

        items.add(stringVal)
        notifyDataSetChanged()

    }

    fun clearItems(){

        notifyDataSetChanged()

    }

    fun addAll(queries: ArrayList<String>) {

        if (!queries.isNullOrEmpty()) {

            for (query in queries) {

                add(query)

            }

        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //val button: CustomCheckBox = itemView.findViewById(R.id.radioButton)
        //val text:AppTextView = itemView.findViewById(R.id.textLabel)

    }

    interface ClickRcycleItemListener {
        fun clickRcycleItemListener(BigId : String)
    }


}