package com.company.sindirella.helpers

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.company.sindirella.R
import com.company.sindirella.activitys.LoginActivity
import com.company.sindirella.activitys.MainActivity
import com.company.sindirella.appPreferences
import com.company.sindirella.fragments.BrowseFragment
import com.company.sindirella.response.Product.ProductResponse
import com.company.sindirella.response.PromotionArea.PromotionAreaResponse
import top.defaults.drawabletoolbox.DrawableBuilder

class BrowserFragmentViewPagerAdapter(private var context: Context, private var data: ProductResponse?) : PagerAdapter() {

    private var mainLayout: ConstraintLayout? = null
    private var clothesView: RelativeLayout? = null
    private var imageViewClothes: ImageView? = null
    private var imageLike: ImageView? = null
    private var brandName: TextView? = null
    private var clothesName: TextView? = null
    private var priceLayout: LinearLayout? = null
    private var pricesNew: TextView? = null
    private var viewLinePrices: View? = null
    private var pricesOld: TextView? = null
    private var examinedButton: Button? = null

    private var viewButton: View? = null

    var clickViewPagerItemListener: ClickViewPagerItemListener? = null


    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.browser_viewpager_view_two,container,false)

        clothesView = layout.findViewById(R.id.clothesView)
        imageViewClothes = layout.findViewById(R.id.imageViewClothes)
        imageLike = layout.findViewById(R.id.imageLike)
        brandName = layout.findViewById(R.id.brandName)
        clothesName = layout.findViewById(R.id.clothesName)
        priceLayout = layout.findViewById(R.id.priceLayout)
        pricesNew = layout.findViewById(R.id.pricesNew)
        viewLinePrices = layout.findViewById(R.id.viewLinePrices)
        pricesOld = layout.findViewById(R.id.pricesOld)
        mainLayout = layout.findViewById(R.id.mainLayout)
        examinedButton = layout.findViewById(R.id.examinedButton)
        viewButton = layout.findViewById(R.id.viewButton)


        var requestOptions = RequestOptions()
        requestOptions.transform(CenterCrop(), RoundedCorners(20))

        Glide.with(context).load(data?.hydraMember?.get(position)?.pictures?.get(0)?.imageUrl).apply(requestOptions).into(imageViewClothes!!)
        //imageViewClothes?.background = DrawableBuilder().cornerRadii(50,50,0,0).build()
        brandName?.text = data?.hydraMember?.get(position)?.brand?.name

        var lengthName = data?.hydraMember?.get(position)?.name?.length

        /*if (lengthName!! > 20){

            clothesName?.textSize = 13f

        }else if (lengthName > 30){

            clothesName?.textSize = 12f

        }

         */

        clothesName?.text = data?.hydraMember?.get(position)?.name
        pricesNew?.text = data?.hydraMember?.get(position)?.priceRental.toString() + " ₺"
        pricesOld?.text = data?.hydraMember?.get(position)?.priceSale.toString() + " ₺"
        pricesOld?.paintFlags = (Paint.STRIKE_THRU_TEXT_FLAG)

        mainLayout?.setOnClickListener {

            clickViewPagerItemListener?.clickViewPagerItem(data?.hydraMember?.get(position)?.BigId.toString())
            //popularClick?.popularClick(productResponse.hydraMember?.get(position)?.BigId)

        }

        imageLike?.setOnClickListener {

            if (appPreferences.token.equals("")){
                val intent = Intent(context, LoginActivity::class.java)
                context.startActivity(intent)
            }else{

            }


        }

        setUI()


        container.addView(layout)

        return layout
    }

    fun setUI(){

        //imageViewClothes?.background = DrawableBuilder().cornerRadius(50).build()
        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).cornerRadius(20).build()
        examinedButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.black)).cornerRadius(20).build()
        viewButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.black)).cornerRadius(20).build()

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {

        return data?.hydraMember?.size!!
    }

    interface ClickViewPagerItemListener {
        fun clickViewPagerItem(BigId: String)
    }


}