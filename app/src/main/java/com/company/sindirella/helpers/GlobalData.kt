package com.company.sindirella.helpers

object GlobalData {

    var base_url = "https://api.sindirella.com/api/"
    var dynamic_url = "https://api.sindirella.com/api/"
    var browse_clothes = "https://api.sindirella.com/api/"
    var promotionAreaClickUrl : String? = null

    var productDetailActivitiesId = -1
    var loginEmail = ""

}