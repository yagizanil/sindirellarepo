package com.company.sindirella.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.company.sindirella.R
import com.company.sindirella.response.Color.ColorsResponseHydraMember
import com.company.sindirella.response.SizeGroup.SizeGroupResponseHydraMember
import kotlinx.android.synthetic.main.filter_size_groups_gird_view.view.*
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class FilterSizeGroupsBaseAdapter(var context: Context, var sizeGroupsData: ArrayList<SizeGroupResponseHydraMember>) : BaseAdapter() {

    var sizeGroupsLayout: RelativeLayout? = null
    var sizeNameText: TextView? = null


    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view = LayoutInflater.from(context).inflate(R.layout.filter_size_groups_gird_view,parent,false)

        sizeGroupsLayout = view.findViewById(R.id.sizeGroupsLayout)
        sizeNameText = view.findViewById(R.id.sizeNameText)

        sizeGroupsLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).strokeWidth(5).strokeColor(ContextCompat.getColor(context,R.color.black)).cornerRadius(10).build()

        sizeNameText?.setTextColor(ContextCompat.getColor(context,R.color.black))
        sizeNameText?.text = sizeGroupsData.get(position).name

        view.sizeGroupsLayout.setOnClickListener {



        }

        /*if (colorData.get(position).hex?.length!! > 4){

            colorLayout?.background = DrawableBuilder().solidColor(Color.parseColor("#${colorData.get(position).hex}")).startColor(context.resources.getColor(
                R.color.black)).strokeWidth(2).cornerRadius(10).build()

        }

         */




        return view

    }

    override fun getItem(position: Int): Any {
        return sizeGroupsData.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return sizeGroupsData.size
    }


}