package com.company.sindirella.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.RelativeLayout
import com.company.sindirella.R
import com.company.sindirella.response.Color.ColorsResponseHydraMember
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class FilterColorBaseAdapter(var context: Context,var colorData: ArrayList<ColorsResponseHydraMember>) : BaseAdapter() {

    private var colorLayout: RelativeLayout? = null


    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view = LayoutInflater.from(context).inflate(R.layout.filter_color_grid_view,parent,false)

        colorLayout = view.findViewById(R.id.colorLayout)

        if (colorData.get(position).hex?.length!! > 4){

            colorLayout?.background = DrawableBuilder().solidColor(Color.parseColor("#${colorData.get(position).hex}")).startColor(context.resources.getColor(R.color.black)).strokeWidth(2).cornerRadius(10).build()

        }




        return view

    }

    override fun getItem(position: Int): Any {
        return colorData.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return colorData.size
    }


}