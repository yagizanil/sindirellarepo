package com.company.sindirella.helpers

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.company.sindirella.R
import com.company.sindirella.response.Activity.ActivitysResponse
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import java.util.ArrayList
import top.defaults.drawabletoolbox.DrawableBuilder

class HomeFragmentWhichActivityAdapter(private val context: Context?,var activity: AppCompatActivity?,private var activitiesData: ActivitysResponse) : RecyclerView.Adapter<HomeFragmentWhichActivityAdapter.ViewHolder>() {

    private var imageViewIcon : ImageView? = null
    private var textViewName : TextView? = null
    private var items: ArrayList<String> = ArrayList()
    private var mainLayout : LinearLayout? = null

    var whichRecycleClick: WhichRecycleClick? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.home_which_activity_gridview, parent, false)


        imageViewIcon = v.findViewById<ImageView>(R.id.imageViewIcon)
        textViewName = v.findViewById<TextView>(R.id.textViewName)
        mainLayout = v.findViewById(R.id.mainLayout)


        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.white)).cornerRadius(50).build()


        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var uri = Uri.parse(activitiesData.hydraMember?.get(position)?.imageUrl)

        GlideToVectorYou.justLoadImage(activity,uri,imageViewIcon)
        //Glide.with(context!!).load(activitiesData.hydraMember?.get(position)?.imageUrl).into(imageViewIcon!!)
        textViewName?.text = activitiesData.hydraMember?.get(position)?.name

        mainLayout?.setOnClickListener {

            whichRecycleClick?.whichClick(activitiesData.hydraMember?.get(position)?.id)

        }

        holder.setIsRecyclable(false)
    }

    override fun getItemCount(): Int {

        return activitiesData.hydraMember?.size!!

    }

    fun add(stringVal: String) {

        items.add(stringVal)
        notifyDataSetChanged()

    }

    fun clearItems(){

        notifyDataSetChanged()

    }

    fun addAll(queries: ArrayList<String>) {

        if (!queries.isNullOrEmpty()) {

            for (query in queries) {

                add(query)

            }

        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //val button: CustomCheckBox = itemView.findViewById(R.id.radioButton)
        //val text:AppTextView = itemView.findViewById(R.id.textLabel)

    }

    interface WhichRecycleClick {
        fun whichClick(activitiesId: Int?)
    }

}
