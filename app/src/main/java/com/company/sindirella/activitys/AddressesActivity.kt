package com.company.sindirella.activitys

import android.content.Intent
import android.media.Image
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.ClientAddressController
import com.company.sindirella.response.ClientAddress.GetClientAddressResponse
import com.company.sindirella.response.ClientAddress.GetClientAddressResponseHydraMembers
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.view.AddressesCardView
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import top.defaults.drawabletoolbox.DrawableBuilder

class AddressesActivity : AppCompatActivity(),ClientAddressController.ClientAddressesListener,AddressesCardView.DeleteAddresListener {

    private var closeImage: ImageView? = null
    private var addAddressesButton: Button? = null
    private var addessesDetailLayout: LinearLayout? = null


    private var clientAddressController: ClientAddressController? = null


    override fun onResume() {

        clientAddressController?.getClientAddresses(appPreferences.token,null)
        super.onResume()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addresses)
        supportActionBar?.hide()


        closeImage = findViewById(R.id.closeImage)
        addAddressesButton = findViewById(R.id.addAddressesButton)
        addessesDetailLayout = findViewById(R.id.addessesDetailLayout)

        clientAddressController = ClientAddressController(this,null)
        clientAddressController?.clientAddressesListener = this
        clientAddressController?.getClientAddresses(appPreferences.token,null)






        closeImage?.setOnClickListener {

            onBackPressed()

        }

        addAddressesButton?.setOnClickListener {

            val intent = Intent(applicationContext,AddressesDetailActivity::class.java)
            startActivity(intent)

        }

        setUI()

    }

    fun setUI(){

        addAddressesButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(applicationContext,R.color.sindirella_blue)).cornerRadius(20).build()

    }

    override fun getClientAddresses(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        addessesDetailLayout?.removeAllViews()

        if (response){

            var addressesData = Gson().fromJson(jsonElement,GetClientAddressResponse::class.java)

            if (!addressesData.hydraMember.isNullOrEmpty()){

                for (i in 0..addressesData.hydraMember?.size!!-1){

                    val view = AddressesCardView(applicationContext)
                    view.deleteAddresListener = this
                    view.setData(addressesData.hydraMember?.get(i))

                    addessesDetailLayout?.addView(view)


                }

            }



        }else{

        }

    }

    override fun deleteClientAddresses(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            clientAddressController?.getClientAddresses(appPreferences.token,null)


        }else{

            clientAddressController?.getClientAddresses(appPreferences.token,null)

        }

    }

    override fun deleteAddresId(id: Int?) {

        if (id != null){

            clientAddressController?.deleteClientAddreses(appPreferences.token,id.toString())

        }

    }



    override fun postClientAddresses(
        response: Boolean,
        jsonElement: JsonElement?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {

    }

}