package com.company.sindirella.activitys

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.company.sindirella.R
import top.defaults.drawabletoolbox.DrawableBuilder

class OrderSummary : AppCompatActivity() {

    private var imageClothes : ImageView? = null
    private var clothesView : View? = null
    private var cargoTrackingButton : Button? = null
    private var extraditionRequestButton : Button? = null
    private var productReportButton : Button? = null
    private var salesContractButton : Button? = null
    private var returnConditionsButton : Button? = null


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_summary)
        supportActionBar?.hide()

        imageClothes = findViewById(R.id.imageClothes)
        clothesView = findViewById(R.id.clothesView)
        cargoTrackingButton = findViewById(R.id.cargoTrackingButton)
        extraditionRequestButton = findViewById(R.id.extraditionRequestButton)
        productReportButton = findViewById(R.id.productReportButton)
        salesContractButton = findViewById(R.id.salesContractButton)
        returnConditionsButton = findViewById(R.id.returnConditionsButton)






        setUI()

    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        clothesView?.background = DrawableBuilder().solidColor(resources.getColor(R.color.white)).cornerRadius(50).build()
        cargoTrackingButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(50).build()

        extraditionRequestButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(50).build()

        productReportButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(50).build()

        salesContractButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(50).build()

        returnConditionsButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(50).build()

    }


}