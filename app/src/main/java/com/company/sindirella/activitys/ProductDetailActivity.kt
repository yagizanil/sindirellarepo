package com.company.sindirella.activitys

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.DynamicApiController
import com.company.sindirella.controller.ProductController
import com.company.sindirella.helpers.ProductDetailViewPagerAdapter
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.Product.ProductDetailResponse
import com.company.sindirella.response.Product.ProductIdResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import top.defaults.drawabletoolbox.DrawableBuilder
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeoutException

class ProductDetailActivity : AppCompatActivity(), ProductController.ProductListener,DynamicApiController.DynamicApiListener {

    private var backIcon : TextView? = null
    private var shareIcon: ImageView? = null
    private var likeIcon: ImageView? = null
    private var pageTitle: TextView? = null
    private var brandName: TextView? = null
    private var productName: TextView? = null
    private var viewPager: ViewPager? = null
    private var sizeTitle: TextView? = null
    private var spinnerSize: Spinner? = null
    private var whichDaysTitle: TextView? = null
    private var startDateText: TextView? = null
    private var endDateText: TextView? = null
    private var productDetailTitle: TextView? = null
    private var productDescription: TextView? = null
    private var productSizeTitle: TextView? = null
    private var productSize: TextView? = null
    private var productColorTitle: TextView? = null
    private var productColor: TextView? = null
    private var productRentTitle: TextView? = null
    private var productRent: TextView? = null
    private var productSalesTitle: TextView? = null
    private var productSales: TextView? = null
    private var productCategoriesTitle: TextView? = null
    private var productCategories: TextView? = null
    private var productEventsTitle: TextView? = null
    private var productEvents: TextView? = null
    private var stylistNoteTitle: TextView? = null
    private var stylistNote: TextView? = null
    private var commentSize: TextView? = null
    private var openCommentButton: Button? = null
    private var returnConditionsButton: Button? = null
    private var confidentialityAgreementButton: Button? = null
    private var pricesOld: TextView? = null
    private var pricesNew: TextView? = null
    private var addBasketButton: Button? = null
    private var webView: WebView? = null

    private var productDetailViewPagerAdapter: ProductDetailViewPagerAdapter? = null

    var productId: String? = null
    var productIdUrl: String? = null

    private var productController: ProductController? = null
    private var dynamicApiController: DynamicApiController? = null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        supportActionBar?.hide()


        backIcon = findViewById(R.id.backIcon)
        shareIcon = findViewById(R.id.shareIcon)
        likeIcon = findViewById(R.id.likeIcon)
        pageTitle = findViewById(R.id.pageTitle)
        brandName = findViewById(R.id.brandName)
        productName = findViewById(R.id.productName)
        viewPager = findViewById(R.id.viewPager)
        sizeTitle = findViewById(R.id.sizeTitle)
        spinnerSize = findViewById(R.id.spinnerSize)
        whichDaysTitle = findViewById(R.id.whichDaysTitle)
        startDateText = findViewById(R.id.startDateText)
        endDateText = findViewById(R.id.endDateText)
        productDetailTitle = findViewById(R.id.productDetailTitle)
        productDescription = findViewById(R.id.productDescription)
        productSizeTitle = findViewById(R.id.productSizeTitle)
        productSize = findViewById(R.id.productSize)
        productColorTitle = findViewById(R.id.productColorTitle)
        productColor = findViewById(R.id.productColor)
        productRentTitle = findViewById(R.id.productRentTitle)
        productRent = findViewById(R.id.productRent)
        productSalesTitle = findViewById(R.id.productSalesTitle)
        productSales = findViewById(R.id.productSales)
        productCategoriesTitle = findViewById(R.id.productCategoriesTitle)
        productCategories = findViewById(R.id.productCategories)
        productEventsTitle = findViewById(R.id.productEventsTitle)
        productEvents = findViewById(R.id.productEvents)
        stylistNoteTitle = findViewById(R.id.stylistNoteTitle)
        stylistNote = findViewById(R.id.stylistNote)
        commentSize = findViewById(R.id.commentSize)
        openCommentButton = findViewById(R.id.openCommentButton)
        returnConditionsButton = findViewById(R.id.returnConditionsButton)
        confidentialityAgreementButton = findViewById(R.id.confidentialityAgreementButton)
        pricesOld = findViewById(R.id.pricesOld)
        pricesNew = findViewById(R.id.pricesNew)
        addBasketButton = findViewById(R.id.addBasketButton)
        webView = findViewById(R.id.webView)





        productId = intent.getStringExtra("id")

        if (productId != null){

            dynamicApiController = DynamicApiController(this,null)
            dynamicApiController?.dynamicApiListener = this
            dynamicApiController?.dynamicApi(productId.toString(),null,null,null,null)

            //productController = ProductController(this,null)
            //productController?.productListener = this
            //productController?.productId(productId)

        }


        returnConditionsButton?.setOnClickListener {

            val intent = Intent(this,MenuWebViewActivity::class.java)
            intent.putExtra("code","cayma-hakki")
            startActivity(intent)

        }

        confidentialityAgreementButton?.setOnClickListener {

            val intent = Intent(this,MenuWebViewActivity::class.java)
            intent.putExtra("code","gizlilik-sozlesmesi")
            startActivity(intent)

        }

        backIcon?.setOnClickListener {

            onBackPressed()

        }




        var arr = ArrayList<String>()

        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")

        //adapter = ProductDetailViewPagerAdapter(this,arr)
        //viewPager?.adapter = adapter








        var controllVal = 0

        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ROOT)
        var calendarStart = Calendar.getInstance()
        var calendarEnd = Calendar.getInstance()

        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            if (controllVal == 1) {

                calendarStart.set(Calendar.YEAR, year)
                calendarStart.set(Calendar.MONTH, monthOfYear)
                calendarStart.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                startDateText?.text = sdf.format(calendarStart.time)
                startDateText?.setTextColor(resources.getColor(R.color.black))

            } else {

                calendarEnd.set(Calendar.YEAR, year)
                calendarEnd.set(Calendar.MONTH, monthOfYear)
                calendarEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                endDateText?.text = sdf.format(calendarEnd.time)
                endDateText?.setTextColor(resources.getColor(R.color.black))

            }
        }

        startDateText?.setOnClickListener {
            controllVal = 1
            DatePickerDialog(this, R.style.DatePickerTheme, date, calendarStart.get(Calendar.YEAR), calendarStart.get(
                Calendar.MONTH), calendarStart.get(Calendar.DAY_OF_MONTH)).show()
        }

        endDateText?.setOnClickListener {
            controllVal = 2
            DatePickerDialog(this, R.style.DatePickerTheme, date, calendarEnd.get(Calendar.YEAR), calendarEnd.get(
                Calendar.MONTH), calendarEnd.get(Calendar.DAY_OF_MONTH)).show()
        }

        openCommentButton?.setOnClickListener {

            val intent = Intent(this,CommentsActivity::class.java)
            intent.putExtra("productId",productId)
            intent.putExtra("brandName",brandName?.text)
            intent.putExtra("productName",productName?.text)
            intent.putExtra("productIdUrl",productIdUrl)
            startActivity(intent)

        }

        likeIcon?.setOnClickListener {

            if (appPreferences.token.equals("")){
                val intent = Intent(this,LoginActivity::class.java)
                startActivity(intent)
            }else{

            }

        }

        addBasketButton?.setOnClickListener {

            if (appPreferences.token.equals("")){
                val intent = Intent(this,LoginActivity::class.java)
                startActivity(intent)
            }else{

            }

        }



        setUI()

    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        //commentButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.black)).cornerRadius(50).build()
        returnConditionsButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(20).build()
        confidentialityAgreementButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(20).build()
        addBasketButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.sindirella_blue)).cornerRadius(20).build()
        openCommentButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(this,R.color.black)).cornerRadius(20).build()

        //startDateText?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(50).build()
        //endDateView?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(50).build()
        //spinnerView?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(50).build()

    }


    @RequiresApi(Build.VERSION_CODES.N)
    override fun getDynamicApi(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                var productDetail = Gson().fromJson(jsonElement,ProductDetailResponse::class.java)

                productId = productDetail.id.toString()
                productIdUrl = productDetail.BigId

                brandName?.text = productDetail.brand?.name
                productName?.text = productDetail.name

                productDetailViewPagerAdapter = ProductDetailViewPagerAdapter(applicationContext,productDetail.pictures)
                viewPager?.adapter = productDetailViewPagerAdapter

                viewPager?.pageMargin = 30
                viewPager?.setPadding(50,0,50,0)
                viewPager?.clipToPadding = false
                viewPager?.offscreenPageLimit = 0


                webView?.settings?.loadWithOverviewMode = true
                webView?.settings?.builtInZoomControls = false
                webView?.isScrollContainer = true
                webView?.settings?.pluginState = WebSettings.PluginState.ON
                webView?.webChromeClient = WebChromeClient()
                webView?.settings?.mediaPlaybackRequiresUserGesture = true

                var inventorySizes = ArrayList<String>()

                for (i in 0..productDetail.inventories?.size!!-1){

                    if (inventorySizes.isNullOrEmpty()){

                        inventorySizes.add(productDetail.inventories?.get(i)?.sizeGroup?.name.toString())

                    }else if (inventorySizes.size > 0){

                        var invertoryControl = false

                        for (j in 0..inventorySizes.size-1){

                            if (productDetail.inventories?.get(i)?.sizeGroup?.name.equals(inventorySizes.get(j))){

                                invertoryControl = true

                            }

                        }

                        if (invertoryControl != true){

                            inventorySizes.add(productDetail.inventories?.get(i)?.sizeGroup?.name.toString())

                        }

                    }

                }

                var adapter = ArrayAdapter<String>(applicationContext, R.layout.spinner_item, R.id.spinnerTextView, inventorySizes)
                spinnerSize?.adapter = adapter



                productDescription?.text = Html.fromHtml(productDetail.description,Html.FROM_HTML_MODE_COMPACT)
                stylistNote?.text = Html.fromHtml(productDetail.notes,Html.FROM_HTML_MODE_COMPACT)

                //webView?.loadDataWithBaseURL(null,productDetail.description,"text/html","UTF-8",null)
                //webView?.setBackgroundColor(resources.getColor(R.color.view_color))

                productRent?.text = productDetail.priceRental.toString() + " ₺"
                productSales?.text = productDetail.priceSale.toString() + " ₺"

                pricesNew?.text = productDetail.priceRental.toString() + " ₺"
                pricesOld?.text = productDetail.priceSale.toString() + " ₺"
                pricesOld?.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG

                commentSize?.text = productDetail.comments?.size.toString() + " yorum"


                var sizeName = ""
                for (i in 0..productDetail.inventories?.size!!-1){

                    if (productDetail.inventories?.get(i)?.sizeGroup != null){

                        sizeName = sizeName + productDetail.inventories?.get(i)?.sizeGroup?.name + " "

                    }

                }
                productSize?.text = sizeName


                productColor?.text = productDetail.color?.name


                var categories = ""
                for (i in 0..productDetail.categories?.size!!-1){

                    categories = "${categories}${productDetail.categories?.get(i)?.name} \n"

                }
                productCategories?.text = categories

                var events = ""
                for (i in 0..productDetail.activities?.size!!-1){

                    events = events + productDetail.activities?.get(i)?.name + "\n"

                }
                productEvents?.text = events


            }else{


            }

        }else{


        }

    }

    override fun getProductId(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                val productIdData = Gson().fromJson(jsonElement,ProductIdResponse::class.java)

                if (!productIdData.hydraMember.isNullOrEmpty()){


                }


            }else{


            }

        }else{


        }

    }

    override fun getProduct(
        response: Boolean?,
        jsonElement: JsonElement?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {
        TODO("Not yet implemented")
    }


}