package com.company.sindirella.activitys

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.fragments.BrowseFragment
import com.company.sindirella.fragments.HomeFragment
import com.company.sindirella.fragments.MenuFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    var navigationView: BottomNavigationView? = null


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        appPreferences.init(applicationContext)

        navigationView = findViewById(R.id.navigationView)

        navigationView?.menu?.add(Menu.NONE, 1, Menu.NONE, resources.getString(R.string.home_fragment))?.setIcon(R.drawable.home_icon)

        navigationView?.menu?.add(Menu.NONE, 2, Menu.NONE, resources.getString(R.string.browse_fragment))?.setIcon(R.drawable.browse_icon)

        navigationView?.menu?.add(Menu.NONE, 3, Menu.NONE, resources.getString(R.string.basket_fragment))?.setIcon(R.drawable.basket_icon)

        navigationView?.menu?.add(Menu.NONE, 4, Menu.NONE, resources.getString(R.string.menu_fragment))?.setIcon(R.drawable.menu_icon)


        changeFragment(HomeFragment(this))




        navigationView?.setOnNavigationItemSelectedListener(object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {

                val f = supportFragmentManager.findFragmentById(R.id.container)

                when(item.itemId){

                    1 ->

                        if (f !is HomeFragment) {
                            val homeFragment = HomeFragment(this@MainActivity)
                            changeFragment(homeFragment)
                            //changeBottomNaviColor(bottomNavigationView?.menu, 0, bottomNavigationView?.menu?.size()!!-1, unselectedColor, selectedColor, item.itemId-1)
                            navigationView?.menu?.getItem(0)?.isChecked = true
                        }

                    2 ->

                        if (f !is BrowseFragment) {

                            val browseFragment = BrowseFragment(this@MainActivity)
                            changeFragment(browseFragment)
                            //changeBottomNaviColor(bottomNavigationView?.menu, 0, bottomNavigationView?.menu?.size()!!-1, unselectedColor, selectedColor, item.itemId-1)
                        }

                    3 -> {

                        if (appPreferences.token.equals("")){
                            val intent = Intent(applicationContext,LoginActivity::class.java)
                            startActivity(intent)
                        }else{
                            val intent = Intent(applicationContext, BasketActivity::class.java)
                            startActivity(intent)
                        }

                    }

                    4 ->

                        if (f !is MenuFragment) {
                            val menuFragment = MenuFragment()
                            changeFragment(menuFragment)
                            //changeBottomNaviColor(bottomNavigationView?.menu, 0, bottomNavigationView?.menu?.size()!!-1, unselectedColor, selectedColor, item.itemId-1)
                        }


                }

                return true
            }

        })






        setUI()

    }


    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }



    fun changeFragment(baseFragment: Fragment) {

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, baseFragment)
        transaction.addToBackStack(baseFragment.toString())
        transaction.commit()

    }

    override fun onDestroy() {

        //appPreferences.token = ""

        super.onDestroy()

    }

    override fun onStop() {

        //appPreferences.token = ""
        super.onStop()
    }


}
