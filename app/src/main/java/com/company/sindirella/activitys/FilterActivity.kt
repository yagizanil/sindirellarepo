package com.company.sindirella.activitys

import android.app.ActionBar
import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.company.sindirella.R
import com.company.sindirella.controller.*
import com.company.sindirella.helpers.*
import com.company.sindirella.response.Activity.ActivitysResponse
import com.company.sindirella.response.Brand.BrandsResponse
import com.company.sindirella.response.Category.CategoriesResponse
import com.company.sindirella.response.Color.ColorsResponse
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.Size.SizeResponse
import com.company.sindirella.response.SizeGroup.SizeGroupResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import top.defaults.drawabletoolbox.DrawableBuilder
import java.text.SimpleDateFormat
import java.util.*

class FilterActivity : AppCompatActivity(),CategoryController.CategoriesListener, SizeController.SizesListener, ColorController.ColorsListener, BrandController.BrandsListener, ActivityController.ActivitiesListener, SizeGroupController.SizeGroupListener {

    private var closeImage: ImageView? = null
    private var pageTitle: TextView? = null
    private var categoriesTitle: TextView? = null
    private var categoriesSpinner: Spinner? = null
    private var sizeTitle: TextView? = null
    private var sizeGrid: GridView? = null
    private var colorTitle: TextView? = null
    private var colorGrid: GridView? = null
    private var brandTitle: TextView? = null
    private var brandSpinner: Spinner? = null
    private var whichDaysTitle: TextView? = null
    private var startDateText: TextView? = null
    private var endDateText: TextView? = null
    private var priceRangeTitle: TextView? = null
    private var startPriceEdit: EditText? = null
    private var endPriceEdit: EditText? = null
    private var activitySubjectTitle: TextView? = null
    private var activitySubjectGrid: GridView? = null
    private var applyButton: Button? = null


    private var categoriesSpinnerArray : ArrayList<String>? = null
    private var brandsSpinnerArray: ArrayList<String>? = null


    private var categoryController: CategoryController? = null
    private var sizeController: SizeController? = null
    private var colorController: ColorController? = null
    private var brandController: BrandController? = null
    private var activityController: ActivityController? = null
    private var sizeGroupController: SizeGroupController? = null


    private var filterSizeGroupsBaseAdapter: FilterSizeGroupsBaseAdapter? = null
    private var filterColorBaseAdapter: FilterColorBaseAdapter? = null
    private var filterEventsGridViewAdapter : FilterEventsGridViewAdapter? = null


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        supportActionBar?.hide()

        closeImage = findViewById(R.id.closeImage)
        pageTitle = findViewById(R.id.pageTitle)
        categoriesTitle = findViewById(R.id.categoriesTitle)
        categoriesSpinner = findViewById(R.id.categoriesSpinner)
        sizeTitle = findViewById(R.id.sizeTitle)
        sizeGrid = findViewById(R.id.sizeGrid)
        colorTitle = findViewById(R.id.colorTitle)
        colorGrid = findViewById(R.id.colorGrid)
        brandTitle = findViewById(R.id.brandTitle)
        brandSpinner = findViewById(R.id.brandSpinner)
        whichDaysTitle = findViewById(R.id.whichDaysTitle)
        startDateText = findViewById(R.id.startDateText)
        endDateText = findViewById(R.id.endDateText)
        priceRangeTitle = findViewById(R.id.priceRangeTitle)
        startPriceEdit = findViewById(R.id.startPriceEdit)
        endPriceEdit = findViewById(R.id.endPriceEdit)
        activitySubjectTitle = findViewById(R.id.activitySubjectTitle)
        activitySubjectGrid = findViewById(R.id.activitySubjectGrid)
        applyButton = findViewById(R.id.applyButton)



        categoryController = CategoryController(this,null)
        categoryController?.categoriesListener = this
        categoryController?.categories(null,null)

        sizeController = SizeController(this,null)
        sizeController?.sizesListener = this
        sizeController?.sizes(null,null,null)

        colorController = ColorController(this,null)
        colorController?.colorsListener = this
        colorController?.colors(null)

        brandController = BrandController(this,null)
        brandController?.brandsListener = this
        brandController?.brands(null)

        activityController = ActivityController(this,null)
        activityController?.activitiesListener = this
        activityController?.activities(null)

        sizeGroupController = SizeGroupController(this,null)
        sizeGroupController?.sizeGroupListener = this
        sizeGroupController?.sizeGroup(null)




        closeImage?.setOnClickListener {

            onBackPressed()

        }




        var arr = resources.getStringArray(R.array.spinner_default_value)

        var adapter = ArrayAdapter<String>(this, R.layout.spinner_item, R.id.spinnerTextView, arr)

        //spinner?.adapter = adapter



        //bedenGrid?.numColumns = 6
        //bedenGrid?.horizontalSpacing = 10
        //bedenGrid?.verticalSpacing = 10

        var arrSize = ArrayList<String>()

        arrSize.add("2")
        arrSize.add("4")
        arrSize.add("6")
        arrSize.add("10")
        arrSize.add("12")
        arrSize.add("14")
        arrSize.add("16")
        arrSize.add("30")
        arrSize.add("32")
        arrSize.add("34")
        arrSize.add("36")
        arrSize.add("38")
        arrSize.add("S")
        arrSize.add("M")
        arrSize.add("L")
        arrSize.add("XL")
        arrSize.add("XLL")

        //var params = bedenGrid?.layoutParams

        if (arrSize.size > 12){
            //params?.height = 300
        }

        //bedenGrid?.layoutParams = params

        //adapterGrid = FilterActivityGridViewAdapter(this,arrSize)

        //bedenGrid?.adapter = adapterGrid





        var controllVal = 0

        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ROOT)
        var calendarStart = Calendar.getInstance()
        var calendarEnd = Calendar.getInstance()

        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            if (controllVal == 1) {

                calendarStart.set(Calendar.YEAR, year)
                calendarStart.set(Calendar.MONTH, monthOfYear)
                calendarStart.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                startDateText?.text = sdf.format(calendarStart.time)
                startDateText?.setTextColor(resources.getColor(R.color.black))

            } else {

                calendarEnd.set(Calendar.YEAR, year)
                calendarEnd.set(Calendar.MONTH, monthOfYear)
                calendarEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                endDateText?.text = sdf.format(calendarEnd.time)

            }
        }

        startDateText?.setOnClickListener {
            controllVal = 1
            DatePickerDialog(this, R.style.DatePickerTheme, date, calendarStart.get(Calendar.YEAR), calendarStart.get(
                Calendar.MONTH), calendarStart.get(Calendar.DAY_OF_MONTH)).show()
        }

        startDateText?.setOnClickListener {
            controllVal = 2
            DatePickerDialog(this, R.style.DatePickerTheme, date, calendarEnd.get(Calendar.YEAR), calendarEnd.get(
                Calendar.MONTH), calendarEnd.get(Calendar.DAY_OF_MONTH)).show()
        }


        /*backPressedText?.setOnClickListener {

            onBackPressed()

        }

         */



        setUI()


    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        applyButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(this,R.color.sindirella_blue)).cornerRadius(20).build()

    }

    override fun getCategories(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                categoriesSpinnerArray = ArrayList()

                var categoriesData = Gson().fromJson(jsonElement,CategoriesResponse::class.java)

                if (!categoriesData.hydraMember.isNullOrEmpty()){

                    for (i in 0..categoriesData.hydraMember?.size!!-1){

                        categoriesSpinnerArray?.add(categoriesData.hydraMember?.get(i)?.name.toString())

                    }


                    var spinnerAdapter = ArrayAdapter(this,R.layout.spinner_item, R.id.spinnerTextView,categoriesSpinnerArray)

                    categoriesSpinner?.adapter = spinnerAdapter


                }




            }else{


            }


        }else{


        }

    }

    override fun getSizes(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                var sizesData = Gson().fromJson(jsonElement,SizeResponse::class.java)




            }



        }else{


        }

    }

    override fun getColors(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                var colorData = Gson().fromJson(jsonElement,ColorsResponse::class.java)

                if (!colorData.hydraMember.isNullOrEmpty()){

                    filterColorBaseAdapter = FilterColorBaseAdapter(this,colorData.hydraMember!!)

                    colorGrid?.adapter = filterColorBaseAdapter

                    colorGrid?.numColumns = 6
                    colorGrid?.horizontalSpacing = 10
                    colorGrid?.verticalSpacing = 20

                }


            }else{



            }



        }else{



        }

    }

    override fun getBrands(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                brandsSpinnerArray = ArrayList()

                var brandsData = Gson().fromJson(jsonElement,BrandsResponse::class.java)

                if (!brandsData.hydraMember.isNullOrEmpty()){

                    for (i in 0..brandsData.hydraMember?.size!!-1){

                        brandsSpinnerArray?.add(brandsData.hydraMember?.get(i)?.name.toString())

                    }

                    var spinnerAdapter = ArrayAdapter(this,R.layout.spinner_item, R.id.spinnerTextView,brandsSpinnerArray)

                    brandSpinner?.adapter = spinnerAdapter


                }


            }else{


            }

        }else{



        }

    }

    override fun getActivities(response: Boolean?, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response == true){

            //val collectionType = object : TypeToken<ArrayList<ActivitysResponse>>(){}.type
            //gson.fromJson(it,collectionType)

            //var activitiesData = ArrayList<ActivitysResponse>()

            //activitiesData = Gson().fromJson(jsonArray,collectionType)

            var activitiesData = Gson().fromJson(jsonElement, ActivitysResponse::class.java)

            if (!activitiesData.hydraMember.isNullOrEmpty()){

                filterEventsGridViewAdapter = FilterEventsGridViewAdapter(this,activitiesData.hydraMember!!)

                activitySubjectGrid?.adapter = filterEventsGridViewAdapter

                activitySubjectGrid?.numColumns = 2
                activitySubjectGrid?.horizontalSpacing = 10
                activitySubjectGrid?.verticalSpacing = 10

            }



        }else{



        }

    }

    override fun getSizeGroup(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                var sizeGroups = Gson().fromJson(jsonElement,SizeGroupResponse::class.java)

                if (!sizeGroups.hydraMember.isNullOrEmpty()){

                    filterSizeGroupsBaseAdapter = FilterSizeGroupsBaseAdapter(this,sizeGroups.hydraMember!!)

                    sizeGrid?.adapter = filterSizeGroupsBaseAdapter

                    sizeGrid?.numColumns = 6
                    sizeGrid?.horizontalSpacing = 10
                    sizeGrid?.verticalSpacing = 20

                }

            }

        }else{


        }

    }

}