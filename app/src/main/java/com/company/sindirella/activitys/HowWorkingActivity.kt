package com.company.sindirella.activitys

import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.company.sindirella.R
import com.company.sindirella.helpers.HowWorkingViewPagerAdapter
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class HowWorkingActivity : AppCompatActivity() {

    private var howWorkingLayout: ConstraintLayout? = null
    private var viewPagerAdapter: HowWorkingViewPagerAdapter? = null
    private var viewPager: ViewPager? = null
    private var button: Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_how_working)
        supportActionBar?.hide()

        howWorkingLayout = findViewById(R.id.howWorkingLayout)
        button = findViewById<Button>(R.id.button)
        viewPager = findViewById<ViewPager>(R.id.viewPagerStarter)

        var arr = ArrayList<String>()
        arr.add("dogukan")
        arr.add("kır")
        arr.add("dogu")

        viewPagerAdapter = HowWorkingViewPagerAdapter(this,arr)
        viewPager?.adapter = viewPagerAdapter






        button?.setOnClickListener {

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

        }

        setUI()

    }


    fun setUI(){

        button?.background = DrawableBuilder().solidColor(ContextCompat.getColor(this,R.color.splah_status_bar)).cornerRadius(50).build()
        viewPager?.background = DrawableBuilder().cornerRadius(60).build()

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

    }

}