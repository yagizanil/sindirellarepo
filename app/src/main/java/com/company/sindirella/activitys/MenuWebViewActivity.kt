package com.company.sindirella.activitys

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.company.sindirella.R
import com.company.sindirella.controller.HelpPageController
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.HelpPage.HelpPageResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import top.defaults.drawabletoolbox.DrawableBuilder

class MenuWebViewActivity : AppCompatActivity(), HelpPageController.HelpPagesListener {

    private var closeImage: ImageView? = null
    private var webView: WebView? = null

    private var code: String? = null

    private var helpPageController: HelpPageController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_webview)
        supportActionBar?.hide()

        closeImage = findViewById(R.id.closeImage)
        webView = findViewById(R.id.webView)

        webView?.settings?.loadWithOverviewMode = true
        webView?.settings?.builtInZoomControls = false
        webView?.isScrollContainer = true
        webView?.settings?.pluginState = WebSettings.PluginState.ON
        webView?.webChromeClient = WebChromeClient()
        webView?.settings?.mediaPlaybackRequiresUserGesture = true


        code = intent.getStringExtra("code")


        helpPageController = HelpPageController(this,null)
        helpPageController?.helpPagesListener = this

        if (code != null){
            helpPageController?.helpPages(code,null,null)
        }

        closeImage?.setOnClickListener {

            onBackPressed()

        }

        setUI()

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        
    }

    override fun getHelpPages(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                var webViewData = Gson().fromJson(jsonElement,HelpPageResponse::class.java)

                if (!webViewData.hydraMember.isNullOrEmpty()){

                    webView?.loadDataWithBaseURL(null,webViewData.hydraMember?.get(0)?.content,"text/html","UTF-8",null)

                }


            }else{



            }

        }else{



        }

    }


}