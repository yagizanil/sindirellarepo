package com.company.sindirella.activitys

import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.CityController
import com.company.sindirella.controller.ClientController
import com.company.sindirella.controller.CountryController
import com.company.sindirella.helpers.GlobalData
import com.company.sindirella.response.City.CitiesResponse
import com.company.sindirella.response.Client.GetMeResponse
import com.company.sindirella.response.Client.PutMeResponse
import com.company.sindirella.response.Client.PutPasswordResponse
import com.company.sindirella.response.Country.CountriesResponse
import com.company.sindirella.response.ErrorResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.util.ArrayList

class SettingsActivity : AppCompatActivity(),ClientController.MeListener,ClientController.PasswordListener,CityController.CitiesListener,CountryController.CountriesListener {

    private var closeImage: ImageView? = null
    private var nameSurnameEdit: EditText? = null
    private var birthdayEdit: EditText? = null
    private var phoneEdit: EditText? = null
    private var citySpinner: Spinner? = null
    private var measureSpinner: Spinner? = null
    private var bodySpinner: Spinner? = null
    private var changeSaveButton: Button? = null
    private var oldPasswordEdit: EditText? = null
    private var newPasswordEdit: EditText? = null
    private var reNewPasswordEdit: EditText? = null
    private var changeSavePasswordButton: Button? = null

    var cityData: CitiesResponse? = null
    var cityNamesArray: ArrayList<String>? = null
    var selectedCity : Int? = null

    var countryData: CountriesResponse? = null
    var countriesNamesArray: ArrayList<String>? = null
    var selectedCountry: Int? = null

    var selectedSize: Int? = null

    private var clientController: ClientController? = null
    private var cityController: CityController? = null
    private var countryController: CountryController? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        supportActionBar?.hide()

        closeImage = findViewById(R.id.closeImage)
        nameSurnameEdit = findViewById(R.id.nameSurnameEdit)
        birthdayEdit = findViewById(R.id.birthdayEdit)
        phoneEdit = findViewById(R.id.phoneEdit)
        citySpinner = findViewById(R.id.citySpinner)
        measureSpinner = findViewById(R.id.measureSpinner)
        bodySpinner = findViewById(R.id.bodySpinner)
        changeSaveButton = findViewById(R.id.changeSaveButton)
        oldPasswordEdit = findViewById(R.id.oldPasswordEdit)
        newPasswordEdit = findViewById(R.id.newPasswordEdit)
        reNewPasswordEdit = findViewById(R.id.reNewPasswordEdit)
        changeSavePasswordButton = findViewById(R.id.changeSavePasswordButton)

        clientController = ClientController(this,null)
        clientController?.meListener = this
        clientController?.passwordListener = this

        cityController = CityController(this,null)
        cityController?.citiesListener = this
        cityController?.cities("DESC",null,null)
        cityNamesArray = ArrayList()
        cityNamesArray?.add("Şehirler")


        countryController = CountryController(this,null)
        countryController?.countriesListener = this
        countryController?.countries(null)
        countriesNamesArray = ArrayList()
        countriesNamesArray?.add("Ölçü Birimi")


        changeSaveButton?.setOnClickListener {

            var meBody = JsonObject()

            if (nameSurnameEdit?.text.isNullOrEmpty() || birthdayEdit?.text.isNullOrEmpty() || phoneEdit?.text.isNullOrEmpty() || selectedCity == -1 || selectedCountry == -1 || selectedSize == null){

                Toast.makeText(applicationContext,"Tüm alanları doldurmanız gerekiyor.",Toast.LENGTH_LONG).show()

            }else{
                if (!GlobalData.loginEmail.equals("")){
                    meBody.addProperty("email",GlobalData.loginEmail)
                }

                meBody.addProperty("name",nameSurnameEdit?.text.toString())
                meBody.addProperty("birthday",birthdayEdit?.text.toString())
                meBody.addProperty("phone",phoneEdit?.text.toString())

                if (selectedCity != null){
                    meBody.addProperty("city","api/cities/${selectedCity}")
                }

                if (selectedCountry != null){
                    meBody.addProperty("country","api/countries/${selectedCountry}")
                }

                if (selectedSize != null){
                    meBody.addProperty("size","")
                }

                clientController?.putMe(appPreferences.token,meBody)
            }



        }


        citySpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position != 0){

                    selectedCity = cityData?.hydraMember?.get(position)?.id

                }else{
                    selectedCity = -1
                }

            }

        }

        measureSpinner?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position != 0){

                    selectedCountry = countryData?.hydraMember?.get(position)?.id

                }else{
                    selectedCountry = -1
                }

            }

        }


        changeSavePasswordButton?.setOnClickListener {

            var changePasswordBody = JsonObject()

            if (newPasswordEdit?.text.isNullOrEmpty() || reNewPasswordEdit?.text.isNullOrEmpty() || oldPasswordEdit?.text.isNullOrEmpty()){

                Toast.makeText(applicationContext,"Tüm alanları doldurmanız gerekiyor.",Toast.LENGTH_LONG).show()

            }else {

                changePasswordBody.addProperty("password",newPasswordEdit?.text.toString())
                changePasswordBody.addProperty("retypedPassword",reNewPasswordEdit?.text.toString())

                clientController?.putPassword(appPreferences.token,null,changePasswordBody)

            }

        }

        val count = object: CountDownTimer(1000,1000){
            override fun onFinish() {
                clientController?.getMe(appPreferences.token)
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }
        count.start()


        closeImage?.setOnClickListener {

            onBackPressed()

        }


    }

    override fun getCities(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            cityData = Gson().fromJson(jsonElement,CitiesResponse::class.java)

            if (!cityData?.hydraMember.isNullOrEmpty()){

                for (i in 0..cityData?.hydraMember?.size!!-1){

                    cityNamesArray?.add(cityData?.hydraMember?.get(i)?.name.toString())

                }

                var adapter = ArrayAdapter(applicationContext,R.layout.spinner_item,R.id.spinnerTextView,cityNamesArray)

                citySpinner?.adapter = adapter

            }else{


            }

        }else{


        }

    }

    override fun getCountries(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            countryData = Gson().fromJson(jsonElement,CountriesResponse::class.java)

            if (!countryData?.hydraMember.isNullOrEmpty()){

                for (i in 0..countryData?.hydraMember?.size!!-1){

                    countriesNamesArray?.add(countryData?.hydraMember?.get(i)?.name.toString())

                }

            }

            var adapter = ArrayAdapter(applicationContext,R.layout.spinner_item,R.id.spinnerTextView,countriesNamesArray)

            measureSpinner?.adapter = adapter


        }else{

        }

    }

    override fun putMe(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            var putMeData = Gson().fromJson(jsonElement,PutMeResponse::class.java)

            if (putMeData != null){
                Toast.makeText(applicationContext,"Değişiklikler Başarılı !",Toast.LENGTH_LONG).show()
            }

        }else{

            Toast.makeText(applicationContext,"Hata Oluştu.",Toast.LENGTH_LONG).show()

        }

    }

    override fun putPassword(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            var passwordData = Gson().fromJson(jsonElement,PutPasswordResponse::class.java)

            passwordData

            Toast.makeText(applicationContext,"Şifre Değiştirilmiştir.",Toast.LENGTH_LONG).show()

        }else{

            Toast.makeText(applicationContext,"Hata Olmuştur.",Toast.LENGTH_LONG).show()

        }

    }

    override fun getMe(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            var meData = Gson().fromJson(jsonElement,GetMeResponse::class.java)

            if (meData != null){

                nameSurnameEdit?.setText(meData.name,TextView.BufferType.EDITABLE)
                birthdayEdit?.setText(meData.birthday,TextView.BufferType.EDITABLE)
                phoneEdit?.setText(meData.phone,TextView.BufferType.EDITABLE)

                if (cityData != null){

                    for (i in 0..cityData?.hydraMember?.size!!-1){

                        if (meData.city?.id == cityData?.hydraMember?.get(i)?.id){

                            citySpinner?.setSelection(i+1)

                        }

                    }

                }

                if (countryData != null){

                    for (i in 0..countryData?.hydraMember?.size!!-1){

                        if (meData.country?.id == countryData?.hydraMember?.get(i)?.id){

                            measureSpinner?.setSelection(i+1)

                        }

                    }

                }

            }



        }else{


        }

    }

    override fun getCitiesId(
        response: Boolean,
        jsonElement: JsonElement?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {



    }




}