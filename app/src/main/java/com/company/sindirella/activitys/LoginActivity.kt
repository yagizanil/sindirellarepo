package com.company.sindirella.activitys

import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.ClientController
import com.company.sindirella.controller.LoginController
import com.company.sindirella.helpers.GlobalData
import com.company.sindirella.response.Client.PostLoginResponse
import com.company.sindirella.response.Client.PostRegisterResponse
import com.company.sindirella.response.ErrorResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import top.defaults.drawabletoolbox.DrawableBuilder

class LoginActivity : AppCompatActivity(), ClientController.onPostRegisterListener,ClientController.onPostLoginListener {

    private var closeImage: ImageView ?= null
    private var helloText: TextView? = null
    private var singUpText: TextView? = null
    private var ePostaText: TextView? = null
    private var mailEdit: EditText? = null
    private var passwordText: TextView? = null
    private var passwordEdit: EditText? = null
    private var birthdayDateText: TextView? = null
    private var dateStartText: TextView? = null
    private var descriptionText: TextView? = null
    private var continueButton: Button? = null
    private var googleConnectButton: Button? = null
    private var beforeLoginText: TextView? = null
    private var loginButton: Button? = null

    var registerBody = JsonObject()
    var loginBody = JsonObject()

    private var loginClick = false

    private var clientController: ClientController? = null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        closeImage = findViewById(R.id.closeImage)
        helloText = findViewById(R.id.helloText)
        singUpText = findViewById(R.id.singUpText)
        ePostaText = findViewById(R.id.ePostaText)
        mailEdit = findViewById(R.id.mailEdit)
        passwordText = findViewById(R.id.passwordText)
        passwordEdit = findViewById(R.id.passwordEdit)
        birthdayDateText = findViewById(R.id.birthdayDateText)
        dateStartText = findViewById(R.id.dateStartText)
        descriptionText = findViewById(R.id.descriptionText)
        continueButton = findViewById(R.id.continueButton)
        googleConnectButton = findViewById(R.id.googleConnectButton)
        beforeLoginText = findViewById(R.id.beforeLoginText)
        loginButton = findViewById(R.id.loginButton)



        clientController = ClientController(this,null)
        clientController?.postRegisterListener = this
        clientController?.postLoginListener = this



        loginButton?.setOnClickListener {

            if (loginClick == false){

                singUpText?.text = "Giriş Yapın"
                birthdayDateText?.visibility = View.GONE
                dateStartText?.visibility = View.GONE
                descriptionText?.visibility = View.GONE
                beforeLoginText?.text = "YENİ Mİ ÜYE OLACAKSINIZ?"
                loginButton?.text = "ÜYE OL"

                loginClick = true

            }else{

                singUpText?.text = "Hemen Üye Olun"
                birthdayDateText?.visibility = View.VISIBLE
                dateStartText?.visibility = View.VISIBLE
                descriptionText?.visibility = View.VISIBLE
                beforeLoginText?.text = "ZATEN BİR HESABIN MI VAR"
                loginButton?.text = "GİRİŞ YAP"

                loginClick = false

            }



        }


        continueButton?.setOnClickListener {

            if (loginClick == false){

                registerBody = JsonObject()

                registerBody.addProperty("email",mailEdit?.text.toString())
                registerBody.addProperty("password",passwordEdit?.text.toString())
                registerBody.addProperty("retypedPassword",passwordEdit?.text.toString())

                clientController?.postRegister(registerBody)

            }else{

                loginBody = JsonObject()

                loginBody.addProperty("username",mailEdit?.text.toString())
                loginBody.addProperty("password",passwordEdit?.text.toString())
                loginBody.addProperty("retypedPassword",passwordEdit?.text.toString())

                clientController?.postlogin(loginBody)

            }

            GlobalData.loginEmail = mailEdit?.text.toString()

        }




        closeImage?.setOnClickListener {

            onBackPressed()

        }


        setUI()

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        dateStartText?.background = DrawableBuilder().solidColor(ContextCompat.getColor(applicationContext,R.color.white)).strokeColor(ContextCompat.getColor(applicationContext,R.color.view_color)).strokeWidth(3).cornerRadius(20).build()
        passwordEdit?.background = DrawableBuilder().solidColor(ContextCompat.getColor(applicationContext,R.color.white)).strokeColor(ContextCompat.getColor(applicationContext,R.color.view_color)).strokeWidth(3).cornerRadius(20).build()
        mailEdit?.background = DrawableBuilder().solidColor(ContextCompat.getColor(applicationContext,R.color.white)).strokeColor(ContextCompat.getColor(applicationContext,R.color.view_color)).strokeWidth(3).cornerRadius(20).build()
        loginButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.sindirella_blue)).cornerRadius(20).build()
        continueButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.black)).cornerRadius(20).build()
        googleConnectButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(20).build()


    }

    override fun onPostRegisterListener(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int, error: ErrorResponse?) {

        if (responseOk){

            var registerData = Gson().fromJson(jsonElement,PostRegisterResponse::class.java)

            loginBody = JsonObject()

            loginBody.addProperty("username",mailEdit?.text.toString())
            loginBody.addProperty("password",passwordEdit?.text.toString())
            loginBody.addProperty("retypedPassword",passwordEdit?.text.toString())

            val count = object: CountDownTimer(300,300){
                override fun onFinish() {
                    clientController?.postlogin(loginBody)
                }

                override fun onTick(millisUntilFinished: Long) {

                }

            }
            count.start()

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

            println("yanlış döndü")

        }

    }

    override fun postLoginListener(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            var loginData = Gson().fromJson(jsonElement,PostLoginResponse::class.java)

            appPreferences.token = "Bearer ${loginData.token.toString()}"

            Toast.makeText(applicationContext,"İşleminiz Başarıyla Gerçekleşti",Toast.LENGTH_LONG).show()

            onBackPressed()

        }else{

            Toast.makeText(applicationContext,error?.message,Toast.LENGTH_LONG).show()

            println("yanlış döndü")

        }

    }

}