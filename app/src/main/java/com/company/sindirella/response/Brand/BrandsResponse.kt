package com.company.sindirella.response.Brand

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class BrandsResponse {

    @SerializedName("hydra:member")
    var hydraMember: ArrayList<BrandsResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null

    @SerializedName("hydra:view")
    var hydraView: BrandsResponseHydraView? = null

}

class BrandsResponseHydraMember {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    init {
        id = null
        name = null
    }

}

class BrandsResponseHydraView {

    @SerializedName("hydra:first")
    var hydraFirst : String? = null

    @SerializedName("hydra:last")
    var hydraLast : String? = null

    @SerializedName("hydra:next")
    var hydraNext : String? = null

}