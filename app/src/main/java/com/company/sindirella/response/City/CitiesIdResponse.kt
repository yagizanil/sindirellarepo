package com.company.sindirella.response.City

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class CitiesIdResponse {

    @SerializedName("towns")
    var towns: ArrayList<CitiesIdResponseTowns>? = null

}

class CitiesIdResponseTowns {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

}