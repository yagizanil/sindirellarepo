package com.company.sindirella.response

import com.google.gson.annotations.SerializedName

class ErrorResponse {

    @SerializedName("@context")
    var BigContext: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("hydra:title")
    var hydraTitle: String? = null

    @SerializedName("hydra:description")
    var hydraDescription: String? = null

    @SerializedName("violations")
    var violations: ArrayList<ErrorResponseViolations>? = null

    @SerializedName("code")
    var code: Int? = null

    @SerializedName("message")
    var message: String? = null

}

class ErrorResponseViolations {

    @SerializedName("propertyPath")
    var propertyPath: String? = null

    @SerializedName("message")
    var message: String? = null

}