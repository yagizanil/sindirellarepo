package com.company.sindirella.response.OrderDetail

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class GetOrderDetailResponse {

    @SerializedName("hydra:member")
    var hydraMember : ArrayList<GetOrderDetailResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems : Int? = null

}

class GetOrderDetailResponseHydraMember {

    @SerializedName("@id")
    var BigId : String? = null

    @SerializedName("@type")
    var BigType : String? = null

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("amountTotal")
    var amountTotal : Int? = null

    @SerializedName("days")
    var days : Int? = null

    @SerializedName("product")
    var product : GetOrderDetailResponseHydraMemberProduct? = null

    @SerializedName("fromDateStr")
    var fromDateStr : String? = null

    @SerializedName("toDateStr")
    var toDateStr : String? = null


    init {
        id = null
        amountTotal = null
        days = null
        product = null
    }

}

class GetOrderDetailResponseHydraMemberProduct {

    @SerializedName("@id")
    var BigId : String? = null

    @SerializedName("@type")
    var BigType : String? = null

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("brand")
    var brand : GetOrderDetailResponseHydraMemberProductBrand? = null

    @SerializedName("priceRental")
    var priceRental : Int? = null

    @SerializedName("pictures")
    var pictures : ArrayList<GetOrderDetailResponseHydraMemberProductPictures>? = null

    @SerializedName("priceRentalAlt")
    var priceRentalAlt : String? = null

}

class GetOrderDetailResponseHydraMemberProductBrand {

    @SerializedName("@id")
    var BigId : String? = null

    @SerializedName("@type")
    var BigType : String? = null

    @SerializedName("name")
    var name : String? = null

}

class GetOrderDetailResponseHydraMemberProductPictures {

    @SerializedName("@id")
    var BigId : String? = null

    @SerializedName("@type")
    var BigType : String? = null

    @SerializedName("imageUrl")
    var imageUrl : String? = null

    @SerializedName("originSizeUrl")
    var originSizeUrl : String? = null

    @SerializedName("showOrder")
    var showOrder : Int? = null

}