package com.company.sindirella.response.ClientAddress

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class GetClientAddressResponse {

    @SerializedName("hydra:member")
    var hydraMember : ArrayList<GetClientAddressResponseHydraMembers>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems : Int? = null

}

class GetClientAddressResponseHydraMembers {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("client")
    var client : GetClientAddressResponseHydraMembersClient? = null

    @SerializedName("address")
    var address : String? = null

    @SerializedName("country")
    var country : String? = null

    @SerializedName("county")
    var county : String? = null

    @SerializedName("city")
    var city : String? = null

    @SerializedName("zipCode")
    var zipCode : String? = null

    @SerializedName("district")
    var district : String? = null

    @SerializedName("receiverName")
    var receiverName : String? = null

    @SerializedName("phone")
    var phone : String? = null

    init {
        id = null
        name = null
        client = null
        address = null
        country = null
        county = null
        city = null
        zipCode = null
        district = null
        receiverName = null
        phone = null
    }

}

class GetClientAddressResponseHydraMembersClient {

    @SerializedName("id")
    var id : Int? = null

}