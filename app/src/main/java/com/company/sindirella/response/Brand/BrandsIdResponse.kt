package com.company.sindirella.response.Brand

import com.google.gson.annotations.SerializedName

class BrandsIdResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    init {
        id = null
        name = null
    }

}