package com.company.sindirella.response.Comment

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.ArrayList

class GetCommentsResponse {

    @SerializedName("hydra:member")
    var hydraMember: ArrayList<GetCommentsResponseHydraMembers>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null

}

class GetCommentsResponseHydraMembers: Serializable {

    @SerializedName("@id")
    var BigId: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("body")
    var body: String? = null

    @SerializedName("hasTitle")
    var hasTitle: Boolean? = null

    @SerializedName("client")
    var client: GetCommentsResponseHydraMembersclient? = null

    @SerializedName("product")
    var product: String? = null

    @SerializedName("rating")
    var rating: Int? = null

    @SerializedName("ratingPerc")
    var ratingPerc: Int? = null

    @SerializedName("formattedDate")
    var formattedDate: String? = null

    @SerializedName("medias")
    var medias: ArrayList<GetCommentsResponseHydraMembersMedias>? = null

    init {
        id = null
        title = null
        body = null
        hasTitle = null
        client = null
        product = null
        rating = null
        ratingPerc = null
        formattedDate = null
        medias = null
    }

}

class GetCommentsResponseHydraMembersclient : Serializable {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("extra")
    var extra : ArrayList<String>? = null

    init {
        id = null
        name = null
        extra = null
    }

}

class GetCommentsResponseHydraMembersMedias : Serializable {

    @SerializedName("imageUrl")
    var imageUrl : String? = null

    @SerializedName("originSizeUrl")
    var originSizeUrl : String? = null

    @SerializedName("showOrder")
    var showOrder : Int? = null

    init {
        imageUrl = null
        originSizeUrl = null
        showOrder = null
    }

}