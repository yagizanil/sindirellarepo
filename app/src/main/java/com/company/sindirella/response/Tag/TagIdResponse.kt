package com.company.sindirella.response.Tag

import com.company.sindirella.response.Invertory.ClientInvertory
import com.google.gson.annotations.SerializedName

class TagIdResponse {

    @SerializedName("START_ACTIVITE")
    var START_ACTIVITE: Int? = null

    @SerializedName("START_PASSIVE")
    var START_PASSIVE: Int? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("client")
    var client: ClientInvertory? = null

    @SerializedName("slug")
    var slug: String? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("createdBy")
    var createdBy: String? = null

    @SerializedName("updatedBy")
    var updatedBy: String? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    @SerializedName("updatedAt")
    var updatedAt: String? = null

    @SerializedName("deletedAt")
    var deletedAt: String? = null

    @SerializedName("deleted")
    var deleted: Boolean? = null

    init {
        START_ACTIVITE = null
        START_PASSIVE = null
        id = null
        name = null
        client = null
        slug = null
        status = null
        createdBy = null
        updatedBy = null
        createdAt = null
        updatedAt = null
        deletedAt = null
        deleted = null
    }

}