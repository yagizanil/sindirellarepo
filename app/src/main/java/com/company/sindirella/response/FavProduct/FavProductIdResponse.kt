package com.company.sindirella.response.FavProduct

import com.google.gson.annotations.SerializedName

class FavProductIdResponse {

    @SerializedName("id")
    var id: Int? = null

    //@SerializedName("product")
    //var product: Product? = null

    init {
        id = null
        //product = null
    }

}