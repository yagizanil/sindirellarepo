package com.company.sindirella.response.PromotionArea

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class PromotionAreaResponse {

    @SerializedName("hydra:member")
    var hydraMember: ArrayList<PromotionAreaResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null

    @SerializedName("hydra:view")
    var hydraView: PromotionAreaResponseHydraView? = null

    @SerializedName("hydra:search")
    var hydraSearch: PromotionAreaResponseHydraSearch? = null

}

class PromotionAreaResponseHydraMember {

    @SerializedName("code")
    var code: String? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("buttonTitle")
    var buttonTitle: String? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("summary")
    var summary: String? = null

    @SerializedName("fullImage")
    var fullImage: String? = null

    @SerializedName("imageUrl")
    var imageUrl: String? = null

    @SerializedName("jsUrl")
    var jsUrl: String? = null

    @SerializedName("apiUrl")
    var apiUrl: String? = null

    init {
        code = null
        url = null
        buttonTitle = null
        title = null
        summary = null
        fullImage = null
        imageUrl = null
        jsUrl = null
        apiUrl = null
    }

}

class PromotionAreaResponseHydraView {

    @SerializedName("hydra:first")
    var hydraFirst: String? = null

    @SerializedName("hydra:last")
    var hydraLast: String? = null

    @SerializedName("hydra:next")
    var hydraNext: String? = null

    init {
        hydraFirst = null
        hydraLast = null
        hydraNext = null
    }

}

class PromotionAreaResponseHydraSearch{

    @SerializedName("hydra:template")
    var hydraTemplate: String? = null

    @SerializedName("hydra:variableRepresentation")
    var hydraVariableRepresentation: String? = null

    @SerializedName("hydra:mapping")
    var hydraMapping: ArrayList<PromotionAreaResponseHydraSearchMapping>? = null

}

class PromotionAreaResponseHydraSearchMapping {

    @SerializedName("variable")
    var variable: String? = null

    @SerializedName("property")
    var property: String? = null

    @SerializedName("required")
    var required: Boolean? = null

}