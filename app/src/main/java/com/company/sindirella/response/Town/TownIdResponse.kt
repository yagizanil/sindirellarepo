package com.company.sindirella.response.Town

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class TownIdResponse {

    @SerializedName("districts")
    var districts: ArrayList<TownIdResponseDistricts>? = null

}

class TownIdResponseDistricts {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

}