package com.company.sindirella.response.Category

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class CategoriesResponse {

    @SerializedName("hydra:member")
    var hydraMember : ArrayList<CategoriesResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null

    @SerializedName("hydra:search")
    var hydraSearch: CategoriesResponseHydraSearch? = null

}

class CategoriesResponseHydraMember {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("slug")
    var slug : String? = null

    @SerializedName("parent")
    var parent : CategoriesResponseHydraMember? = null

    init {
        id = null
        name = null
        slug = null
        parent = null
    }

}

class CategoriesResponseHydraSearch {

    @SerializedName("hydra:template")
    var hydraTemplate : String? = null

    @SerializedName("hydra:variableRepresentation")
    var hydraVariableRepresentation : String? = null

    @SerializedName("hydra:mapping")
    var hydraMapping : ArrayList<CategoriesResponseHydraSearchMapping>? = null

}

class CategoriesResponseHydraSearchMapping {

    @SerializedName("variable")
    var variable : String? = null

    @SerializedName("property")
    var property : String? = null

    @SerializedName("required")
    var required : Boolean? = null

}

/*class CategoriesResponseHydraMemberParent {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("slug")
    var slug : String? = null

    "id": 4,
    "name": "ETEK",
    "parent": null,
    "slug": "etek-4"

}*/