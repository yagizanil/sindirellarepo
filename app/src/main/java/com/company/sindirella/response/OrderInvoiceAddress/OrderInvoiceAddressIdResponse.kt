package com.company.sindirella.response.OrderInvoiceAddress

import com.google.gson.annotations.SerializedName

class OrderInvoiceAddressIdResponse {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("theOrder")
    var theOrder: String? = null

    @SerializedName("companyName")
    var companyName: String? = null

    @SerializedName("firstName")
    var firstName: String? = null

    @SerializedName("lastName")
    var lastName: String? = null

    @SerializedName("taxIdentityNumber")
    var taxIdentityNumber: String? = null

    @SerializedName("taxOffice")
    var taxOffice: String? = null

    @SerializedName("country")
    var country: String? = null

    @SerializedName("city")
    var city: String? = null

    @SerializedName("county")
    var county: String? = null

    @SerializedName("district")
    var district: String? = null

    @SerializedName("zipcode")
    var zipcode: String? = null

    @SerializedName("updatedBy")
    var updatedBy: String? = null

    @SerializedName("deletedAt")
    var deletedAt: String? = null

    @SerializedName("deleted")
    var deleted: Boolean? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    @SerializedName("updatedAt")
    var updatedAt: String? = null

    init {
        id = null
        theOrder = null
        companyName = null
        firstName = null
        lastName = null
        taxIdentityNumber = null
        taxOffice = null
        country = null
        city = null
        county = null
        district = null
        zipcode = null
        updatedBy = null
        deletedAt = null
        deleted = null
        createdAt = null
        updatedAt = null

    }

}