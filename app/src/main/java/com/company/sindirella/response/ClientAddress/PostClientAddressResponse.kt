package com.company.sindirella.response.ClientAddress

import com.google.gson.annotations.SerializedName

class PostClientAddressResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("client")
    var client : Client? = null

    @SerializedName("address")
    var address : String? = null

    @SerializedName("country")
    var country : String? = null

    @SerializedName("county")
    var county : String? = null

    @SerializedName("city")
    var city : String? = null

    @SerializedName("zipCode")
    var zipCode : String? = null

    @SerializedName("district")
    var district : String? = null

    @SerializedName("receiverName")
    var receiverName : String? = null

    @SerializedName("phone")
    var phone : String? = null

    init {
        id = null
        name = null
        client = null
        address = null
        country = null
        county = null
        city = null
        zipCode = null
        district = null
        receiverName = null
        phone = null
    }

}

class Client {

    @SerializedName("id")
    var id : Int? = null

}