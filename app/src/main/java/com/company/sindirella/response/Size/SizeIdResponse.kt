package com.company.sindirella.response.Size

import com.google.gson.annotations.SerializedName

class SizeIdResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    init {
        id = null
        name = null
    }

}