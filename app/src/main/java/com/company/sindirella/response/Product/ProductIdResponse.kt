package com.company.sindirella.response.Product

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class ProductIdResponse {

    @SerializedName("hydra:member")
    var hydraMember: ArrayList<ProductIdResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null

}

class ProductIdResponseHydraMember {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("notes")
    var notes: String? = null

    @SerializedName("brand")
    var brand: ProductIdResponseHydraMemberBrand? = null

    @SerializedName("color")
    var color: String? = null

    @SerializedName("isRental")
    var isRental: Boolean? = null

    @SerializedName("priceRental")
    var priceRental: Int? = null

    @SerializedName("priceSale")
    var priceSale: Int? = null

    @SerializedName("vatRate")
    var vatRate: String? = null

    @SerializedName("tags")
    var tags: ArrayList<String>? = null

    @SerializedName("pictures")
    var pictures: ArrayList<ProductIdResponseHydraMemberPictures>? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("fullLink")
    var fullLink: String? = null

    @SerializedName("priceRentalAlt")
    var priceRentalAlt: Int? = null


    init {
        id = null
        name = null
        description = null
        notes = null
        brand = null
        color = null
        isRental = null
        priceRental = null
        priceSale = null
        tags = null
        pictures = null
        url = null
        fullLink = null
        vatRate = null
        priceRentalAlt = null
    }


}

class ProductIdResponseHydraMemberBrand {

    @SerializedName("name")
    var name: String? = null

}

class ProductIdResponseHydraMemberPictures {

    @SerializedName("imageUrl")
    var imageUrl: String? = null

    @SerializedName("originSizeUrl")
    var originSizeUrl: String? = null

    @SerializedName("showOrder")
    var showOrder: Int? = null

}