package com.company.sindirella.response.Client

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class PostLoginResponse {

    @SerializedName("token")
    var token : String? = null

    init {
        token = null
    }



}