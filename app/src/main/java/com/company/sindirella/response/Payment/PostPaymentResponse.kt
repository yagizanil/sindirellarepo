package com.company.sindirella.response.Payment

import com.google.gson.annotations.SerializedName

class PostPaymentResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("theOrder")
    var theOrder : String? = null

    @SerializedName("amount")
    var amount : Int? = null

    init {
        id = null
        theOrder = null
        amount = null
    }

}