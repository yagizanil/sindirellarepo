package com.company.sindirella.response.Product

import com.company.sindirella.response.Activity.ActivitysResponseHydraMember
import com.company.sindirella.response.Activity.ActivitysResponseHydraSearch
import com.company.sindirella.response.Activity.ActivitysResponseHydraSearchMapping
import com.company.sindirella.response.Activity.ActivitysResponseHydraView
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class ProductResponse {

    @SerializedName("hydra:member")
    var hydraMember: ArrayList<ProductResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null

    @SerializedName("hydra:view")
    var hydraView: ProductResponseHydraView? = null

    @SerializedName("hydra:search")
    var hydraSearch: ProductResponseHydraSearch? = null

}

class ProductResponseHydraMember {

    @SerializedName("@id")
    var BigId: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("notes")
    var notes: String? = null

    @SerializedName("brand")
    var brand: ProductResponseHydraMemberBrand? = null

    @SerializedName("color")
    var color: String? = null

    @SerializedName("isRental")
    var isRental: Boolean? = null

    @SerializedName("priceRental")
    var priceRental: Int? = null

    @SerializedName("priceSale")
    var priceSale: Int? = null

    @SerializedName("vatRate")
    var vatRate: Int? = null

    @SerializedName("tags")
    var tags: ArrayList<String>? = null

    @SerializedName("pictures")
    var pictures: ArrayList<ProductResponseHydraMemberPictures>? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("fullLink")
    var fullLink: String? = null

    @SerializedName("priceRentalAlt")
    var priceRentalAlt: Int? = null

    init {
        id = null
        name = null
        description = null
        notes = null
        brand = null
        color = null
        isRental = null
        priceRental = null
        priceSale = null
        vatRate = null
        tags = null
        pictures = null
        url = null
        fullLink = null
        priceRentalAlt = null
        BigId = null
        BigType = null
    }

}

class ProductResponseHydraMemberBrand {

    @SerializedName("name")
    var name: String? = null

}

class ProductResponseHydraMemberPictures {

    @SerializedName("imageUrl")
    var imageUrl: String? = null

    @SerializedName("originSizeUrl")
    var originSizeUrl: String? = null

    @SerializedName("showOrder")
    var showOrder: Int? = null

}

class ProductResponseHydraView {

    @SerializedName("hydra:first")
    var hydraFirst: String? = null

    @SerializedName("hydra:last")
    var hydraLast: String? = null

    @SerializedName("hydra:next")
    var hydraNext: String? = null

    init {
        hydraFirst = null
        hydraLast = null
        hydraNext = null
    }

}

class ProductResponseHydraSearch{

    @SerializedName("hydra:template")
    var hydraTemplate: String? = null

    @SerializedName("hydra:variableRepresentation")
    var hydraVariableRepresentation: String? = null

    @SerializedName("hydra:mapping")
    var hydraMapping: ArrayList<ProductResponseHydraSearchMapping>? = null

}

class ProductResponseHydraSearchMapping {

    @SerializedName("variable")
    var variable: String? = null

    @SerializedName("property")
    var property: String? = null

    @SerializedName("required")
    var required: Boolean? = null

}