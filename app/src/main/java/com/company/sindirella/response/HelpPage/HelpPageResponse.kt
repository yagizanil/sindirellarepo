package com.company.sindirella.response.HelpPage

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class HelpPageResponse {


    @SerializedName("hydra:member")
    var hydraMember: ArrayList<HelpPageResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null

}

class HelpPageResponseHydraMember {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("content")
    var content: String? = null

    @SerializedName("title")
    var title: String? = null

    init {
        id = null
        content = null
        title = null
    }

}