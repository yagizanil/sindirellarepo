package com.company.sindirella.response.Order

//import com.company.sindirella.response.Order.InvoiceAddress
//import com.company.sindirella.response.Order.OrderDetail
import com.company.sindirella.response.Order.Payment
//import com.company.sindirella.response.Order.ShipmentAddress
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class OrdersResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("orderedAt")
    var orderedAt : String? = null

    @SerializedName("amountNet")
    var amountNet : Int? = null

    @SerializedName("amountVat")
    var amountVat : Int? = null

    @SerializedName("amountShipment")
    var amountShipment : Int? = null

    @SerializedName("amountTotal")
    var amountTotal : Int? = null

    //@SerializedName("orderDetails")
    //var orderDetails : ArrayList<OrderDetail>? = null

    @SerializedName("payment")
    var payment : Payment? = null

    //@SerializedName("shipmentAddress")
    //var shipmentAddress : ShipmentAddress? = null

    //@SerializedName("invoiceAddress")
    //var invoiceAddress : InvoiceAddress? = null

    @SerializedName("status")
    var status : Int? = null

    @SerializedName("statusStr")
    var statusStr : String? = null

    @SerializedName("transactionId")
    var transactionId : String? = null

    init {
        id = null
        orderedAt = null
        amountNet = null
        amountVat = null
        amountShipment = null
        amountTotal = null
        //orderDetails = null
        payment = null
        //shipmentAddress = null
        //invoiceAddress = null
        status = null
        statusStr = null
        transactionId = null
    }

}