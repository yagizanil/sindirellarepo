package com.company.sindirella.response.City

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class CitiesResponse {

    @SerializedName("hydra:member")
    var hydraMember: ArrayList<CitiesResponseHydraMember>? = null

    @SerializedName("hydra:totalItem")
    var hydraTotalItem: Int? = null



}

class CitiesResponseHydraMember {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    init {
        id = null
        name = null
    }

}