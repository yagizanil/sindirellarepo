package com.company.sindirella.response.Comment

import com.google.gson.annotations.SerializedName

class CommentsIdResponse {

    @SerializedName("product")
    var product: String? = null

    init {
        product = null
    }

}