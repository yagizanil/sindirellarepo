package com.company.sindirella.response.Invertory

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class InventoriesIdResponse {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("product")
    var product: String? = null

    @SerializedName("size")
    var size: String? = null

    @SerializedName("barcode")
    var barcode: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("statusStr")
    var statusStr: String? = null

    @SerializedName("note")
    var note: String? = null

    @SerializedName("client")
    var client: ClientInvertory? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    @SerializedName("updatedAt")
    var updatedAt: String? = null

    @SerializedName("deletedAt")
    var deletedAt: String? = null

    @SerializedName("deleted")
    var deleted: Boolean? = null

    @SerializedName("createdBy")
    var createdBy: String? = null

    @SerializedName("updatedBy")
    var updatedBy: String? = null

    init {
        id = null
        product = null
        size = null
        barcode = null
        status = null
        statusStr = null
        note = null
        client = null
        createdAt = null
        updatedAt = null
        deletedAt = null
        deleted = null
        createdBy = null
        updatedBy = null
    }

}

class ClientInvertory {

    @SerializedName("roles")
    var roles : ArrayList<String>? = null

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("email")
    var email : String? = null

    @SerializedName("phone")
    var phone : String? = null

    @SerializedName("password")
    var password : String? = null

    @SerializedName("socialFbId")
    var socialFbId : String? = null

    @SerializedName("socialGoogleId")
    var socialGoogleId : String? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("birthday")
    var birthday : String? = null

    @SerializedName("lastLogin")
    var lastLogin : String? = null

    @SerializedName("extra")
    var extra : ArrayList<String>? = null

    @SerializedName("clientAddresses")
    var clientAddresses : ArrayList<String>? = null

    @SerializedName("orders")
    var orders : ArrayList<String>? = null

    @SerializedName("basketItems")
    var basketItems : ArrayList<String>? = null

    @SerializedName("payments")
    var payments : ArrayList<String>? = null

    @SerializedName("invoices")
    var invoices : ArrayList<String>? = null

    @SerializedName("salt")
    var salt : String? = null

    @SerializedName("favProducts")
    var favProducts : ArrayList<String>? = null

    @SerializedName("city")
    var city : String? = null

    @SerializedName("size")
    var size : String? = null

    @SerializedName("country")
    var country : String? = null

    @SerializedName("comments")
    var comments : ArrayList<String>? = null

    @SerializedName("deletedAt")
    var deletedAt : String? = null

    @SerializedName("deleted")
    var deleted : Boolean? = null

    @SerializedName("createdAt")
    var createdAt : String? = null

    @SerializedName("updatedAt")
    var updatedAt : String? = null

    init {
        roles = null
        password = null
        salt = null
        id = null
        email = null
        phone = null
        socialFbId = null
        socialGoogleId = null
        name = null
        birthday = null
        lastLogin = null
        extra = null
        clientAddresses = null
        orders = null
        basketItems = null
        payments = null
        invoices = null
        favProducts = null
        city = null
        size = null
        country = null
        comments = null
        deletedAt = null
        deleted = null
        createdAt = null
        updatedAt = null
    }

}