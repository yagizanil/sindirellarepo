package com.company.sindirella.response.Payment

import com.google.gson.annotations.SerializedName

class GetPaymentIdResponse {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("theOrder")
    var theOrder: String? = null

    @SerializedName("client")
    var client: String? = null

    @SerializedName("amount")
    var amount: Int? = null

    @SerializedName("transactionId")
    var transactionId: String? = null

    @SerializedName("responseRaw")
    var responseRaw: String? = null

    @SerializedName("responseToken")
    var responseToken: String? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("extra")
    var extra: String? = null

    @SerializedName("invoice")
    var invoice: String? = null

    @SerializedName("type")
    var type: Int? = null

    @SerializedName("typeStr")
    var typeStr: String? = null

    @SerializedName("deletedAt")
    var deletedAt: String? = null

    @SerializedName("deleted")
    var deleted: Boolean? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    @SerializedName("updatedAt")
    var updatedAt: String? = null

    init {
        id = null
        theOrder = null
        client = null
        amount = null
        transactionId = null
        responseRaw = null
        responseToken = null
        status = null
        extra = null
        invoice = null
        type = null
        typeStr = null
        deletedAt = null
        deleted = null
        createdAt = null
        updatedAt = null

    }

}