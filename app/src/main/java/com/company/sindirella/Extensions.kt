package com.company.sindirella

import android.content.Context
import android.content.SharedPreferences


var mContext: Context? = null

object appPreferences {



    private const val NAME = "Sindirella"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences


    fun init(context: Context) {
        preferences = context.getSharedPreferences(NAME, MODE)
    }
    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }
    var token:String
        get() = preferences.getString("token","")!!
        set(value) = preferences.edit {
            it.putString("token",value)
        }
    var userObject:String
        get() = preferences.getString("GenericUserObject","")!!
        set(value) = preferences.edit {
            it.putString("GenericUserObject",value)
        }
    var countTingObject:String
        get() = preferences.getString("CountingObject","")!!
        set(value) = preferences.edit {
            it.putString("CountingObject",value)
        }

    var browseGridOrRecycle:Boolean
        get() = preferences.getBoolean("grid",false)
        set(value) = preferences.edit {
            it.putBoolean("grid",value)
        }

}

