package com.company.sindirella.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.company.sindirella.R
import com.company.sindirella.activitys.*
import com.company.sindirella.controller.HelpPageController
import com.company.sindirella.view.MenuItemView
import com.google.gson.JsonElement
import java.util.ArrayList

class MenuFragment : Fragment() {

    private var itemLayout : LinearLayout? = null




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.fragment_menu,container,false)

        itemLayout = view.findViewById(R.id.itemLayout)







        var menuArray = ArrayList<String>()

        menuArray.add("BİZİ TANIYIN")
        menuArray.add("BASIN")
        menuArray.add("İLETİŞİM")
        menuArray.add("SIKÇA SORULAN SORULAR")
        menuArray.add("ÜYELİK SÖZLEŞMESİ")
        menuArray.add("GİZLİLİK SÖZLEŞMESİ")
        menuArray.add("NASIL KİRALARIM")
        menuArray.add("RANDEVU")
        menuArray.add("TESLİMAT & İADE")
        menuArray.add("SATIŞ SÖZLEŞMESİ")
        menuArray.add("KİRALAMA SÖZLEŞMESİ")
        menuArray.add("KİŞİSEL VERİLERİN KORUNMASI")

        for (i in 0..menuArray.size-1){

            val menuView = MenuItemView(context)

            menuView.setTitle(menuArray.get(i))

            menuView.setOnClickListener {

                val intent = Intent(context,MenuWebViewActivity::class.java)

                if (menuArray.get(i).equals("BİZİ TANIYIN")){
                    intent.putExtra("code","bizi-taniyin")
                }else if (menuArray.get(i).equals("BASIN")){
                    intent.putExtra("code","basin")
                }else if (menuArray.get(i).equals("İLETİŞİM")){
                    intent.putExtra("code","iletisim")
                }else if (menuArray.get(i).equals("SIKÇA SORULAN SORULAR")){
                    intent.putExtra("code","sikca-sorulan-sorular")
                }else if (menuArray.get(i).equals("ÜYELİK SÖZLEŞMESİ")){
                    intent.putExtra("code","uyelik-sozlesmesi")
                }else if (menuArray.get(i).equals("GİZLİLİK SÖZLEŞMESİ")){
                    intent.putExtra("code","gizlilik-sozlesmesi")
                }else if (menuArray.get(i).equals("NASIL KİRALARIM")){
                    intent.putExtra("code","nasil-kiralarim")
                }else if (menuArray.get(i).equals("RANDEVU")){
                    intent.putExtra("code","randevu")
                }else if (menuArray.get(i).equals("TESLİMAT & İADE")){
                    intent.putExtra("code","cayma-hakki")
                }else if (menuArray.get(i).equals("SATIŞ SÖZLEŞMESİ")){
                    intent.putExtra("code","satis-sozlesmesi")
                }else if (menuArray.get(i).equals("KİRALAMA SÖZLEŞMESİ")){
                    intent.putExtra("code","kiralama-sozlesmesi")
                }else if (menuArray.get(i).equals("KİŞİSEL VERİLERİN KORUNMASI")){
                    intent.putExtra("code","kisisel-veriler-kanunu")
                }

                startActivity(intent)


                /*if (menuArray.get(i).equals("Yeni Gelenler")){
                    val intent = Intent(context, LoginActivity::class.java)
                    startActivity(intent)
                }else if (menuArray.get(i).equals("Sonbahar")){
                    val intent = Intent(context, OrderSummary::class.java)
                    startActivity(intent)
                }else if (menuArray.get(i).equals("İletişim")){
                    val intent = Intent(context, MyAccountActivity::class.java)
                    startActivity(intent)
                }else if (menuArray.get(i).equals("Bizi Tanıyın")){
                    val intent = Intent(context, CommentsActivity::class.java)
                    startActivity(intent)
                }else if (menuArray.get(i).equals("Kullanım Şartları")){
                    val intent = Intent(context, CommentDetail::class.java)
                    startActivity(intent)
                }

                 */



            }

            itemLayout?.addView(menuView)

        }



        
        return view
    }



}