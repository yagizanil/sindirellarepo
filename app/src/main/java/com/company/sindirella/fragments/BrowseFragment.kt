package com.company.sindirella.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.company.sindirella.R
import com.company.sindirella.activitys.FilterActivity
import com.company.sindirella.activitys.MainActivity
import com.company.sindirella.activitys.ProductDetailActivity
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.DynamicApiController
import com.company.sindirella.controller.ProductController
import com.company.sindirella.helpers.BrowseFragmentRecycleAdapter
import com.company.sindirella.helpers.BrowserFragmentViewPagerAdapter
import com.company.sindirella.helpers.GlobalData
import com.company.sindirella.helpers.HomeFragmentPopularAdapter
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.Product.ProductResponse
import com.company.sindirella.view.SortPopUp
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class BrowseFragment(var activity: MainActivity?) : Fragment(),DynamicApiController.DynamicApiListener,SortPopUp.SortClickListener,BrowseFragmentRecycleAdapter.ClickRcycleItemListener,BrowserFragmentViewPagerAdapter.ClickViewPagerItemListener {


    private var titleText : TextView? = null
    private var totalProduct : TextView? = null
    private var filterText : TextView? = null
    private var sortText : TextView? = null
    private var recycler : RecyclerView? = null
    private var adapter : BrowseFragmentRecycleAdapter? = null
    private var adapterViewPager : BrowserFragmentViewPagerAdapter? = null
    private var viewFilter : View? = null
    private var viewSort : View? = null
    private var displayChangeImage : ImageView? = null
    private var clothesViewPager: ViewPager? = null

    var dialogView : SortPopUp? = null

    var productData : ProductResponse? = null

    private var productController: ProductController? = null
    private var dynamicApiController: DynamicApiController? = null



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.fragment_browse_two,container,false)

        dynamicApiController = DynamicApiController(null,this)
        dynamicApiController?.dynamicApiListener = this

        if (GlobalData.productDetailActivitiesId != -1){

            dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,GlobalData.productDetailActivitiesId.toString())

        }else{

            dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,null)

        }

        //productController = ProductController(null,this)
        //productController?.productListener = this
        //productController?.product(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",null)



        recycler = view.findViewById(R.id.recycler)
        viewFilter = view.findViewById(R.id.viewFilter)
        viewSort = view.findViewById(R.id.viewSort)
        sortText = view.findViewById(R.id.sortText)
        filterText = view.findViewById(R.id.filterText)
        displayChangeImage = view.findViewById(R.id.displayChangeImage)
        totalProduct = view.findViewById(R.id.totalProduct)
        clothesViewPager = view.findViewById(R.id.clothesViewPager)




        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)  //fragment içinte tanımlanan

        recycler?.layoutManager = layoutManager
        recycler?.itemAnimator = DefaultItemAnimator()


        displayChangeImage?.setOnClickListener {

            if (!appPreferences.browseGridOrRecycle){
                //adapter = BrowseFragmentRecycleAdapter(context)
                recycler?.visibility = View.VISIBLE
                clothesViewPager?.visibility = View.GONE

                appPreferences.browseGridOrRecycle = true

                adapter = BrowseFragmentRecycleAdapter(context,productData)
                adapter?.clickRcycleItemListener = this
                recycler?.adapter = adapter
                recycler?.itemAnimator = DefaultItemAnimator()
                recycler?.layoutManager = GridLayoutManager(context,2)

            }else{
                //adapter = BrowseFragmentRecycleAdapter(context)
                recycler?.visibility = View.GONE
                clothesViewPager?.visibility = View.VISIBLE

                appPreferences.browseGridOrRecycle = false

                adapterViewPager = BrowserFragmentViewPagerAdapter(context!!,productData)
                adapterViewPager?.clickViewPagerItemListener = this

                clothesViewPager?.adapter = adapterViewPager

                clothesViewPager?.pageMargin = 30
                clothesViewPager?.setPadding(80,0,80,0)
                clothesViewPager?.clipToPadding = false
                clothesViewPager?.offscreenPageLimit = 0

            }

        }


        viewSort?.setOnClickListener {

            var arr = ArrayList<String>()

            dialogView = SortPopUp(context!!,arr)
            dialogView?.sortClickListener = this
            dialogView?.createView(R.layout.sort_pop_up)
            dialogView?.show()
            dialogView?.setCanceledOnTouchOutside(false)


            dialogView?.exitButton?.setOnClickListener {

                dialogView?.dismiss()

            }

        }


        viewFilter?.setOnClickListener {

            val intent = Intent(context,FilterActivity::class.java)
            startActivity(intent)

        }



        setUI()


        return view
    }

    override fun onDestroyView() {

        GlobalData.productDetailActivitiesId = -1
        super.onDestroyView()
    }


    fun setUI(){

        viewFilter?.background = DrawableBuilder().strokeColor(resources.getColor(R.color.black)).strokeWidth(4).cornerRadius(20).build()
        viewSort?.background = DrawableBuilder().strokeColor(resources.getColor(R.color.black)).strokeWidth(4).cornerRadius(20).build()


    }

    override fun getDynamicApi(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                productData = Gson().fromJson(jsonElement,ProductResponse::class.java)

                if(!productData?.hydraMember.isNullOrEmpty()){

                    if (appPreferences.browseGridOrRecycle){

                        recycler?.visibility = View.VISIBLE
                        clothesViewPager?.visibility = View.GONE

                        adapter = BrowseFragmentRecycleAdapter(context,productData)
                        adapter?.clickRcycleItemListener = this
                        recycler?.adapter = adapter
                        recycler?.itemAnimator = DefaultItemAnimator()
                        recycler?.layoutManager = GridLayoutManager(context,2)

                    }else{

                        recycler?.visibility = View.GONE
                        clothesViewPager?.visibility = View.VISIBLE

                        totalProduct?.text = "${productData?.hydraTotalItems} ürün"

                        adapterViewPager = BrowserFragmentViewPagerAdapter(context!!,productData)
                        adapterViewPager?.clickViewPagerItemListener = this

                        clothesViewPager?.adapter = adapterViewPager

                        clothesViewPager?.pageMargin = 30
                        clothesViewPager?.setPadding(80,0,80,0)
                        clothesViewPager?.clipToPadding = false
                        clothesViewPager?.offscreenPageLimit = 0

                    }


                    //recycler?.adapter = adapter

                }


            }


        }else{


        }

    }

    override fun sortClickPopUp(key: String?, sort: String?) {

        when(key){

            "order[popularity]" -> {

                dynamicApiController?.dynamicApi(GlobalData.browse_clothes,sort,null,null,null)

            }

            "order[id]" -> {

                dynamicApiController?.dynamicApi(GlobalData.browse_clothes,null,null,sort,null)

            }

            "order[priceRental]" -> {

                dynamicApiController?.dynamicApi(GlobalData.browse_clothes,null,sort,null,null)

            }


        }

        dialogView?.dismiss()

    }

    override fun clickRcycleItemListener(BigId: String) {
        val intent = Intent(context, ProductDetailActivity::class.java)
        intent.putExtra("id",BigId)
        startActivity(intent)
    }

    override fun clickViewPagerItem(BigId: String) {
        val intent = Intent(context, ProductDetailActivity::class.java)
        intent.putExtra("id",BigId)
        startActivity(intent)
    }


}