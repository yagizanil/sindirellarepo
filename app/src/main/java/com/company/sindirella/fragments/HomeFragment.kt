package com.company.sindirella.fragments

import android.annotation.TargetApi
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.company.sindirella.R
import com.company.sindirella.activitys.LoginActivity
import com.company.sindirella.activitys.MainActivity
import com.company.sindirella.activitys.MyAccountActivity
import com.company.sindirella.activitys.ProductDetailActivity
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.ActivityController
import com.company.sindirella.controller.FavProductController
import com.company.sindirella.controller.ProductController
import com.company.sindirella.controller.PromotionAreaController
import com.company.sindirella.helpers.*
import com.company.sindirella.response.Activity.ActivitysResponse
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.FavProduct.GetFavProductResponse
import com.company.sindirella.response.Product.ProductResponse
import com.company.sindirella.response.PromotionArea.PromotionAreaResponse
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import top.defaults.drawabletoolbox.DrawableBuilder
import java.text.SimpleDateFormat
import java.util.*

class HomeFragment(var activity: MainActivity?) : Fragment(),FavProductController.FavProductListener,PromotionAreaController.PromotionAreasListener,ActivityController.ActivitiesListener,ProductController.ProductListener,HomeFragmentWhichActivityAdapter.WhichRecycleClick, HomeFragmentPopularAdapter.PopularAdapterClickListener {

    private var mainLayout: ConstraintLayout? = null
    private var spinnerAct: Spinner? = null
    private var spinnerDataArray: ArrayList<String>? = null
    private var viewPagerHome : ViewPager? = null
    private var viewPagerAdapter: HomeFragmentViewPagerAdapter? = null
    private var startCalendar : TextView? = null
    private var endCalendar : TextView? = null
    private var recycleWhichActivity: RecyclerView? = null
    private var whichActivityAdapter: HomeFragmentWhichActivityAdapter? = null
    private var recyclePopular: RecyclerView? = null
    private var homeFragmentPopularAdapter: HomeFragmentPopularAdapter? = null
    private var viewSpinner : View? = null
    private var viewStartDate : View? = null
    private var viewEndDate : View? = null
    private var showButton : Button? = null
    private var likeIcon: ImageView? = null
    private var profileIcon: ImageView? = null

    private var promotionAreaController: PromotionAreaController? = null
    private var activityController: ActivityController? = null
    private var productController: ProductController? = null
    private var favProductController: FavProductController? = null


    private var favData: GetFavProductResponse? = null
    private var productData: ProductResponse? = null

    private var postFavBool = false
    private var likePosition: Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        favProductController = FavProductController(null,this)
        favProductController?.favProductListener = this
        favProductController?.getFavProduct(appPreferences.token,null)

        promotionAreaController = PromotionAreaController(null,this)
        promotionAreaController?.promotionAreasListener = this
        promotionAreaController?.promotionAreas(null)

        activityController = ActivityController(null,this)
        activityController?.activitiesListener = this
        activityController?.activities(null)


        mainLayout = view.findViewById(R.id.mainLayout)
        spinnerAct = view.findViewById(R.id.spinnerAct)
        viewPagerHome = view.findViewById(R.id.viewPagerHome)
        startCalendar = view.findViewById(R.id.startCalendar)
        endCalendar = view.findViewById(R.id.endCalendar)
        recycleWhichActivity = view.findViewById(R.id.recycleWhichActivity)
        recyclePopular = view.findViewById(R.id.recyclePopular)
        viewSpinner = view.findViewById(R.id.viewSpinner)
        viewStartDate = view.findViewById(R.id.viewStartDate)
        viewEndDate = view.findViewById(R.id.viewEndDate)
        showButton = view.findViewById(R.id.showButton)
        likeIcon = view.findViewById(R.id.likeIcon)
        profileIcon = view.findViewById(R.id.profileIcon)





        var arrViewPager = ArrayList<String>()

        arrViewPager.add("doğu")
        arrViewPager.add("kan")
        arrViewPager.add("kır")
        arrViewPager.add("doğukan")
        arrViewPager.add("kankır")

        homeFragmentPopularAdapter?.addAll(arrViewPager)  //data yollandı

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)  //fragment içinte tanımlanan





        var controllVal = 0

        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ROOT)
        var calendarStart = Calendar.getInstance()
        var calendarEnd = Calendar.getInstance()

        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            if (controllVal == 1) {

                calendarStart.set(Calendar.YEAR, year)
                calendarStart.set(Calendar.MONTH, monthOfYear)
                calendarStart.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                startCalendar?.text = sdf.format(calendarStart.time)
                startCalendar?.setTextColor(resources.getColor(R.color.black))

            } else {

                calendarEnd.set(Calendar.YEAR, year)
                calendarEnd.set(Calendar.MONTH, monthOfYear)
                calendarEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                endCalendar?.text = sdf.format(calendarEnd.time)

            }
        }

        startCalendar?.setOnClickListener {
            controllVal = 1
            DatePickerDialog(context!!, R.style.DatePickerTheme, date, calendarStart.get(Calendar.YEAR), calendarStart.get(Calendar.MONTH), calendarStart.get(Calendar.DAY_OF_MONTH)).show()
        }

        endCalendar?.setOnClickListener {
            controllVal = 2
            DatePickerDialog(context!!, R.style.DatePickerTheme, date, calendarEnd.get(Calendar.YEAR), calendarEnd.get(Calendar.MONTH), calendarEnd.get(Calendar.DAY_OF_MONTH)).show()
        }



        likeIcon?.setOnClickListener {

            if (appPreferences.token.equals("")){
                val intent = Intent(context,LoginActivity::class.java)
                startActivity(intent)
            }else{

            }

        }

        profileIcon?.setOnClickListener {

            if (appPreferences.token.equals("")){
                val intent = Intent(context,LoginActivity::class.java)
                startActivity(intent)
            }else{
                val intent = Intent(context,MyAccountActivity::class.java)
                startActivity(intent)
            }

        }





        setUI()

        return view
    }


    fun setUI() {

        showButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.show_button)).cornerRadius(20).build()
        viewSpinner?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.view_background)).cornerRadius(50).build()
        viewStartDate?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.view_background)).cornerRadius(50).build()
        viewEndDate?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.view_background)).cornerRadius(50).build()
        mainLayout?.setBackgroundColor(resources.getColor(R.color.how_working_status_bar))
        //viewPagerHome?.background = DrawableBuilder().cornerRadius(50).build()
        //window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        //window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

    }

    override fun getPromotionAreas(response: Boolean?, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response == true){

            //val collectionType = object : TypeToken<ArrayList<PromotionAreaResponse>>(){}.type
            //gson.fromJson(it,collectionType)

            //var promotionArea = ArrayList<PromotionAreaResponse>()

            //promotionArea = Gson().fromJson(jsonArray,collectionType)

            var promotionArea = Gson().fromJson(jsonElement,PromotionAreaResponse::class.java)

            if (!promotionArea.hydraMember.isNullOrEmpty()){
                viewPagerAdapter = HomeFragmentViewPagerAdapter(context!!,activity,promotionArea)

                viewPagerHome?.adapter = viewPagerAdapter

                viewPagerHome?.pageMargin = 30
                viewPagerHome?.setPadding(80,0,80,0)
                viewPagerHome?.clipToPadding = false
                viewPagerHome?.offscreenPageLimit = 0
            }

        }else{



        }

    }


    override fun getActivities(response: Boolean?, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response == true){

            spinnerDataArray = ArrayList()

            //val collectionType = object : TypeToken<ArrayList<ActivitysResponse>>(){}.type
            //gson.fromJson(it,collectionType)

            //var activitiesData = ArrayList<ActivitysResponse>()

            //activitiesData = Gson().fromJson(jsonArray,collectionType)

            var activitiesData = Gson().fromJson(jsonElement,ActivitysResponse::class.java)

            if(!activitiesData.hydraMember.isNullOrEmpty()){

                whichActivityAdapter = HomeFragmentWhichActivityAdapter(context,activity,activitiesData)
                whichActivityAdapter?.whichRecycleClick = this
                recycleWhichActivity?.adapter = whichActivityAdapter
                recycleWhichActivity?.layoutManager = LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false)
                recycleWhichActivity?.itemAnimator = DefaultItemAnimator()
                //whichActivityAdapter?.addAll(arrViewPager)


                for (i in 0..activitiesData.hydraMember?.size!!-1){

                    spinnerDataArray?.add(activitiesData.hydraMember?.get(i)?.name.toString())

                }


                var adapter = ArrayAdapter<String>(context!!, R.layout.spinner_item, R.id.spinnerTextView, spinnerDataArray)

                spinnerAct?.adapter = adapter

            }

        }else{



        }

    }

    override fun whichClick(activitiesId: Int?) {

        activity?.changeFragment(BrowseFragment(activity))
        GlobalData.productDetailActivitiesId = activitiesId!!
        activity?.navigationView?.menu?.getItem(1)?.isChecked = true

    }



    override fun getProduct(response: Boolean?, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response == true){

            //val collectionType = object : TypeToken<ArrayList<ProductResponse>>(){}.type
            //gson.fromJson(it,collectionType)

            //var productData = ArrayList<ProductResponse>()

            //productData = Gson().fromJson(jsonArray,collectionType)

            productData = Gson().fromJson(jsonElement,ProductResponse::class.java)

            if(!productData?.hydraMember.isNullOrEmpty()){

                homeFragmentPopularAdapter = HomeFragmentPopularAdapter(context,productData!!,favData!!)
                homeFragmentPopularAdapter?.popularAdapterClickListener = this
                recyclePopular?.adapter = homeFragmentPopularAdapter
                recyclePopular?.itemAnimator = DefaultItemAnimator()
                recyclePopular?.layoutManager = LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false)

            }

        }else{



        }

    }

    override fun popularClick(BigId: String?) {

        val intent = Intent(context, ProductDetailActivity::class.java)
        intent.putExtra("id",BigId)
        startActivity(intent)

    }

    override fun likeClick(id: Int?, position: Int?) {

        likePosition = position

        var likeBody = JsonObject()

        likeBody.addProperty("product","/api/products/${id}")
        likeBody.addProperty("status",1)

        favProductController?.postFavProduct(appPreferences.token,likeBody)

    }

    override fun disLikeClick(id: Int?, position: Int?) {

        favProductController?.deleteFavProductId(appPreferences.token,id.toString())

    }


    override fun getProductId(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

    }

    override fun getFavProduct(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            if (postFavBool == false){
                favData = Gson().fromJson(jsonElement,GetFavProductResponse::class.java)
            }else{

                favData = Gson().fromJson(jsonElement,GetFavProductResponse::class.java)

                homeFragmentPopularAdapter?.changeFav(favData!!,likePosition)

                postFavBool = false
            }






        }else{



        }

        productController = ProductController(null,this)
        productController?.productListener = this
        productController?.product(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",null)


    }

    override fun postFavProduct(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){
            postFavBool = true

            val count = object: CountDownTimer(300,300){
                override fun onFinish() {
                    favProductController?.getFavProduct(appPreferences.token,null)
                }

                override fun onTick(millisUntilFinished: Long) {

                }

            }
            count.start()


        }else{

            Toast.makeText(context,"Bir Hata Oluştu.",Toast.LENGTH_LONG).show()

        }

    }

    override fun deleteFavProductId(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            favProductController?.getFavProduct(appPreferences.token,null)



        }else{


        }

    }


}